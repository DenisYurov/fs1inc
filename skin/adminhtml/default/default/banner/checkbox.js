jQuery(document).ready(function(){
    jQuery("#bannerLeftGrid_table tr .checkbox").each(function(){
        var val = jQuery(this).val();
        if(!jQuery(this).is(":checked")) {
            jQuery(this).parents().eq(1).find('td:last input').val("");
            jQuery(this).parents().eq(1).find('td:last input').attr("disabled","disabled"); 
        }
    });
});

jQuery(document).on('click', '#bannerLeftGrid_table tr', function(){
    if(!jQuery(this).find('td:first-child input').is(":checked")) {
        jQuery(this).find('td:last-child input').val("");
        jQuery(this).find('td:last-child input').attr("disabled","disabled"); 
    } else {
        jQuery(this).find('td:last-child input').attr("disabled",false);
    }
});