
/**
 * All-In-One Checkout : All-In-One Checkout v1.1.0 (OPCB Unit)
 *
 * @package:     Aitoc_Aitcheckout / Aitoc_Aitcheckout
 * @version      1.4.17 - 1.4.17
 * @license:     FoOiFpEBsy1CQtPSBPTqBFwAOZTnyw3qCMIlTMg96m
 * @copyright:   Copyright (c) 2017 AITOC, Inc. (http://www.aitoc.com)
 */
var AitGiftwrap = Class.create(Step,  
{
    initGiftwrap: function()
    {
        this.initEvents(this.container);
    },

    afterInit: function()
    {
        this.initGiftwrap();
        this.setReloadSteps(['payment', 'review']);

        if (this.isShowCartInCheckout)
        {
            this.addReloadSteps(['messages']);
        }

        if (aitCheckout.getStep('coupon'))
        {
            aitCheckout.getStep('coupon').addReloadSteps(['aitgiftwrap']);
        }

        if (aitCheckout.getStep('review') && this.isShowCartInCheckout)
        {
            aitCheckout.getStep('review').addReloadSteps(['aitgiftwrap']);
        }

        aitCheckout.setStep('aitgiftwrap', this);
    }
    
});