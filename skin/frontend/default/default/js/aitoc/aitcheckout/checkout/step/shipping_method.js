
/**
 * All-In-One Checkout : All-In-One Checkout v1.1.0 (OPCB Unit)
 *
 * @package:     Aitoc_Aitcheckout / Aitoc_Aitcheckout
 * @version      1.4.17 - 1.4.17
 * @license:     FoOiFpEBsy1CQtPSBPTqBFwAOZTnyw3qCMIlTMg96m
 * @copyright:   Copyright (c) 2017 AITOC, Inc. (http://www.aitoc.com)
 */
var AitShippingMethod = Class.create(Step,  
    {
        initShippingMethod: function(shippingMethodContainerId)
        {
            this.initEvents(shippingMethodContainerId);
        },

        afterInit: function()
        {
            this.initShippingMethod(this.ids.loadContainer);

            if (aitCheckout.getStep('shipping'))
            {
                aitCheckout.getStep('shippinglocation').setReloadSteps(['shipping_method']);
            }
            else
            {
                aitCheckout.getStep('billinglocation').setReloadSteps(['shipping_method']);
                aitCheckout.getStep('billinglocation').initVirtualUpdate();
            }

            this.setReloadSteps(['payment', 'review']);
        }
    });