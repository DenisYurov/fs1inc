
function addtocartevent(){
    console.log('add to cart event');
    dataLayer.push({'event': 'GAevent','eventCategory':  'Main-Mobile','eventAction': 'Button-click','eventLabel': 'Add-to-cart-listing-mob',});
}

function addtocartProductViewevent(){
    console.log('prod view event');
    dataLayer.push({'event': 'GAevent','eventCategory':  'Catalog-Mobile','eventAction': 'Button-click','eventLabel': 'Add-to-cart-product-page-mob',});
}

function phoneNumberClickMob(){
    console.log('Phone-number-click-mob');
    dataLayer.push({'event': 'GAevent','eventCategory':  'Header-Mobile','eventAction': 'Button-click','eventLabel': 'Phone-number-click-mob',});
}

function contactMobileClick(){
    console.log('Contact-Mobile Phone-number-click-mob');
   	dataLayer.push({'event': 'GAevent','eventCategory':  'Contact-Mobile','eventAction': 'Button-click','eventLabel': 'Phone-number-click-mob',}); 
}

function bannerclick(eventLabel){
   if( eventLabel =='Start-shopping-click') {
        console.log("bannerclick triggered label is Start-shopping-click-mob");
        dataLayer.push({'event': 'GAevent','eventCategory':  'Main-Mobile','eventAction': 'Button-click','eventLabel': 'Start-shopping-click-mob'
        });

    }else{
        console.log("bannerclick triggered label is "+eventLabel);
        dataLayer.push({'event': 'GAevent','eventCategory': 'Main-Mobile','eventAction': 'Button-click','eventLabel': eventLabel
        }); 

    }
}