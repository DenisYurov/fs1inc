/**
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category        design
 * @package        
 * @copyright       Copyright (c) 2010 Magento Inc. (http://www.magentocommerce.com)
 * @license          https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

/*** All Media Query Styles ***/

/* iPhone 6 (PORTRAIT) */

@media only screen 
and (min-device-width: 375px) 
and (max-device-width: 667px) 
and (orientation: portrait) { 

section#info {
    background: #f3f3f3;
}


.bizinfo {
    font-size: 100%;
}

p.infotext {
    margin: 2% 5% 5% 5%;
    font-size: 85%;
    text-indent: 10px;
    color: #444;
}


footer {
    min-height: 100px;
    border-bottom: 1px solid #ccc;
    background: #eee;
    color: #444;
    text-shadow: 0 1px 1px #f6f6f6;
    background-image: -webkit-gradient(linear,left top,left bottom,from(#fdfdfd),to(#eee));
    background-image: -webkit-linear-gradient(#fdfdfd,#eee);
    background-image: -moz-linear-gradient(#fdfdfd,#eee);
    background-image: -ms-linear-gradient(#fdfdfd,#eee);
    background-image: -o-linear-gradient(#fdfdfd,#eee);
    background-image: linear-gradient(#fdfdfd,#eee);
    border-top: 1px solid #538db9;
}

.footerlinks {
    width: 100%;
    border-bottom: 1px solid #538db9;
    padding: 1% 0 5% 0;
}

.footerlinks ul {   
   line-height: 27px;
   overflow: hidden;
   padding: 2.5%;
   list-style: none;
   display: inline;
   font-size: 3em;
}

.footerlinks ul li {
    display: inline;
    border: none;
}

.footerlinks ul li a {
    color: #1394ca;
    text-decoration: none;
    font-size: 35%;
    font-weight: 700;
}

.footerlinks li::after {
    content: "|";
    font-size: 35%;
    padding-left: 10px;
}

.first.last::after {
    content: "";
}

.container {
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
    margin-bottom: 20px;
}

.partner {
    padding-top: 10px;
    margin-bottom: 10px;
}

.list-inline, .social-inline  {
    margin: 0;
    padding: 0;
}

.list-inline li {
    display: inline-block;
    background: none;
    border: none;
    width: inherit;
    margin: 0;
}

.container ul li:hover {
    border: none;
    width: inherit;
}

.payment.links .partner li img {
    width: 40px;
    height: 25px;
}

.socialcontainer {
    margin-right: auto;
    margin-left: auto;
    padding: 10px 15px 0 0;
}

.social-inline li {
    display: inline-block;
    background: none;
    border: none;
    width: inherit;
    margin: 1% 2% 0 0;
}

.bottombackground {
    margin: 0;
    padding: 0;
    width: 100%;
    font-size: 100%;
}

.bottomcontainer {
    height: 10%;
    border:1px solid #145072;
    background:#2567ab;
    background-image:-webkit-gradient(linear,left top,left bottom,from(#5f9cc5),to(#396b9e));
    background-image:-webkit-linear-gradient(#5f9cc5,#396b9e);
    background-image:-moz-linear-gradient(#5f9cc5,#396b9e);
    background-image:-ms-linear-gradient(#5f9cc5,#396b9e);
    background-image:-o-linear-gradient(#5f9cc5,#396b9e);
    background-image:linear-gradient(#5f9cc5,#396b9e);
}

p.copyright {
    padding: 5% 0 5% 0;
    text-align: center;
    font-size: 80%;
    text-shadow: none;
    color: #fff;
}
}

/* iPhone 6 (Landscape) */

@media only screen 
and (min-device-width: 375px) 
and (max-device-width: 667px) 
and (orientation: landscape) { 

section#info {
    background: #f3f3f3;
}


.bizinfo {
    font-size: 100%;
}

p.infotext {
    margin: 2% 5% 5% 5%;
    font-size: 85%;
    text-indent: 10px;
    color: #444;
}


footer {
    min-height: 100px;
    border-bottom: 1px solid #ccc;
    background: #eee;
    color: #444;
    text-shadow: 0 1px 1px #f6f6f6;
    background-image: -webkit-gradient(linear,left top,left bottom,from(#fdfdfd),to(#eee));
    background-image: -webkit-linear-gradient(#fdfdfd,#eee);
    background-image: -moz-linear-gradient(#fdfdfd,#eee);
    background-image: -ms-linear-gradient(#fdfdfd,#eee);
    background-image: -o-linear-gradient(#fdfdfd,#eee);
    background-image: linear-gradient(#fdfdfd,#eee);
    border-top: 1px solid #538db9;
}

.footerlinks {
    width: 100%;
    border-bottom: 1px solid #538db9;
    padding: 1% 0;
}

.footerlinks ul {   
   line-height: 27px;
   overflow: hidden;
   padding: 2.5%;
   list-style: none;
   display: inline;
}

.footerlinks ul li {
    display: inline;
    border: none;
    margin: 0 2% 0 0;
}

.footerlinks ul li a {
    color: #1394ca;
    text-decoration: none;
    font-size: 80%;
    font-weight: 700;
}

.footerlinks li::after {
    content: "|";
    font-size: 65%;
    padding-left: 10px;
}

.container {
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
    margin-bottom: 20px;
}

.partner {
    padding-top: 10px;
    margin-bottom: 10px;
}

.list-inline, .social-inline  {
    margin: 0;
    padding: 0;
}

.list-inline li {
    display: inline-block;
    background: none;
    border: none;
    width: inherit;
    margin: 0;
}

.container ul li:hover {
    border: none;
    width: inherit;
}

.payment.links .partner li img {
    width: 40px;
    height: 25px;
}

.socialcontainer {
    margin-right: auto;
    margin-left: auto;
    padding: 10px 15px 0 0;
}

.social-inline li {
    display: inline-block;
    background: none;
    border: none;
    width: inherit;
    margin: 1% 2% 0 0;
}

.bottombackground {
    margin: 0;
    padding: 0;
    width: 100%;
    font-size: 100%;
}

.bottomcontainer {
    height: 10%;
    border:1px solid #145072;
    background:#2567ab;
    background-image:-webkit-gradient(linear,left top,left bottom,from(#5f9cc5),to(#396b9e));
    background-image:-webkit-linear-gradient(#5f9cc5,#396b9e);
    background-image:-moz-linear-gradient(#5f9cc5,#396b9e);
    background-image:-ms-linear-gradient(#5f9cc5,#396b9e);
    background-image:-o-linear-gradient(#5f9cc5,#396b9e);
    background-image:linear-gradient(#5f9cc5,#396b9e);
}

p.copyright {
    padding: 5% 0 5% 0;
    text-align: center;
    font-size: 80%;
    text-shadow: none;
    color: #fff;
}
}

/* Samsung Galaxy S5 (Portrait) */

@media only screen 
and (min-device-width: 360px) 
and (max-device-width: 640px) 
and (orientation: portrait) { 

section#info {
    background: #f3f3f3;
}


.bizinfo {
    font-size: 100%;
}

p.infotext {
    margin: 2% 5% 5% 5%;
    font-size: 85%;
    text-indent: 10px;
    color: #444;
}


footer {
    min-height: 100px;
    border-bottom: 1px solid #ccc;
    background: #eee;
    color: #444;
    text-shadow: 0 1px 1px #f6f6f6;
    background-image: -webkit-gradient(linear,left top,left bottom,from(#fdfdfd),to(#eee));
    background-image: -webkit-linear-gradient(#fdfdfd,#eee);
    background-image: -moz-linear-gradient(#fdfdfd,#eee);
    background-image: -ms-linear-gradient(#fdfdfd,#eee);
    background-image: -o-linear-gradient(#fdfdfd,#eee);
    background-image: linear-gradient(#fdfdfd,#eee);
    border-top: 1px solid #538db9;
}

.footerlinks {
    width: 100%;
    border-bottom: 1px solid #538db9;
    padding: 1% 0 5% 0;
}

.footerlinks ul {   
   line-height: 27px;
   overflow: hidden;
   padding: 2.5% 0 0 3.5%;
   list-style: none;
   font-size: 3em;
}

.footerlinks ul li {
    display: inline;
    border: none;
}

.footerlinks ul li a {
    color: #1394ca;
    text-decoration: none;
    font-size: 37%;
    font-weight: 700;
}

.footerlinks li::after {
    content: "|";
    font-size: 35%;
    padding-left: 10px;
}

.first.last::after, .last.privacy::after {
    content: "";
}

.container {
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
    margin-bottom: 20px;
}

.partner {
    padding-top: 10px;
    margin-bottom: 10px;
}

.list-inline, .social-inline  {
    margin: 0;
    padding: 0;
}

.list-inline li {
    display: inline-block;
    background: none;
    border: none;
    width: inherit;
    margin: 0;
}

.container ul li:hover {
    border: none;
    width: inherit;
}

.payment.links .partner li img {
    width: 40px;
    height: 25px;
}

.socialcontainer {
    margin-right: auto;
    margin-left: auto;
    padding: 10px 15px 0 0;
}

.social-inline li {
    display: inline-block;
    background: none;
    border: none;
    width: inherit;
    margin: 1% 2% 0 0;
}

.bottombackground {
    margin: 0;
    padding: 0;
    width: 100%;
    font-size: 100%;
}

.bottomcontainer {
    height: 10%;
    border:1px solid #145072;
    background:#2567ab;
    background-image:-webkit-gradient(linear,left top,left bottom,from(#5f9cc5),to(#396b9e));
    background-image:-webkit-linear-gradient(#5f9cc5,#396b9e);
    background-image:-moz-linear-gradient(#5f9cc5,#396b9e);
    background-image:-ms-linear-gradient(#5f9cc5,#396b9e);
    background-image:-o-linear-gradient(#5f9cc5,#396b9e);
    background-image:linear-gradient(#5f9cc5,#396b9e);
}

p.copyright {
    padding: 5% 0 5% 0;
    text-align: center;
    font-size: 80%;
    text-shadow: none;
    color: #fff;
}
}

/* Samsung Galaxy S5 (Landscape) */

@media only screen 
and (min-device-width: 360px) 
and (max-device-width: 640px) 
and (orientation: landscape) { 

section#info {
    background: #f3f3f3;
}


.bizinfo {
    font-size: 100%;
}

p.infotext {
    margin: 2% 5% 5% 5%;
    font-size: 85%;
    text-indent: 10px;
    color: #444;
}


footer {
    min-height: 100px;
    border-bottom: 1px solid #ccc;
    background: #eee;
    color: #444;
    text-shadow: 0 1px 1px #f6f6f6;
    background-image: -webkit-gradient(linear,left top,left bottom,from(#fdfdfd),to(#eee));
    background-image: -webkit-linear-gradient(#fdfdfd,#eee);
    background-image: -moz-linear-gradient(#fdfdfd,#eee);
    background-image: -ms-linear-gradient(#fdfdfd,#eee);
    background-image: -o-linear-gradient(#fdfdfd,#eee);
    background-image: linear-gradient(#fdfdfd,#eee);
    border-top: 1px solid #538db9;
}

.footerlinks {
    width: 100%;
    border-bottom: 1px solid #538db9;
    padding: 1% 0;
}

.footerlinks ul {   
   line-height: 27px;
   overflow: hidden;
   padding: 2.5%;
   list-style: none;
   display: inline;
}

.footerlinks ul li {
    display: inline;
    border: none;
    margin: 0 2% 0 0;
}

.footerlinks ul li a {
    color: #1394ca;
    text-decoration: none;
    font-size: 80%;
    font-weight: 700;
}

.footerlinks li::after {
    content: "|";
    font-size: 65%;
    padding-left: 10px;
}

.first.last::after {
    content: "";
}

.container {
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
    margin-bottom: 20px;
}

.partner {
    padding-top: 10px;
    margin-bottom: 10px;
}

.list-inline, .social-inline  {
    margin: 0;
    padding: 0;
}

.list-inline li {
    display: inline-block;
    background: none;
    border: none;
    width: inherit;
    margin: 0;
}

.container ul li:hover {
    border: none;
    width: inherit;
}

.payment.links .partner li img {
    width: 40px;
    height: 25px;
}

.socialcontainer {
    margin-right: auto;
    margin-left: auto;
    padding: 10px 15px 0 0;
}

.social-inline li {
    display: inline-block;
    background: none;
    border: none;
    width: inherit;
    margin: 1% 2% 0 0;
}

.bottombackground {
    margin: 0;
    padding: 0;
    width: 100%;
    font-size: 100%;
}

.bottomcontainer {
    height: 10%;
    border:1px solid #145072;
    background:#2567ab;
    background-image:-webkit-gradient(linear,left top,left bottom,from(#5f9cc5),to(#396b9e));
    background-image:-webkit-linear-gradient(#5f9cc5,#396b9e);
    background-image:-moz-linear-gradient(#5f9cc5,#396b9e);
    background-image:-ms-linear-gradient(#5f9cc5,#396b9e);
    background-image:-o-linear-gradient(#5f9cc5,#396b9e);
    background-image:linear-gradient(#5f9cc5,#396b9e);
}

p.copyright {
    padding: 5% 0 5% 0;
    text-align: center;
    font-size: 80%;
    text-shadow: none;
    color: #fff;
}
}
/* Additional Styles (Portrait) */

@media only screen and (max-width: 360px) and (orientation : portrait) {
/* PORTRAIT:
	Apple iPhone 3G
	Apple iPhone 3GS
	Apple iPhone 4
	Apple iPhone 4S
	Apple iPhone 5
	Apple iPod Touch
	BlackBerry Bold 9360
	BlackBerry Bold 9790
	BlackBerry Curve 9320
	BlackBerry Curve 9380
	BlackBerry Torch 9800
	BlackBerry Torch 9810
	HP Veer
	HTC 7 Mozart
	HTC 7 Trophy
	HTC Desire
	HTC Desire C
	HTC Desire HD
	HTC Legend
	HTC One V
	HTC Titan 4G
	HTC Wildfire S
	HTC Windows Phone 8X
	Huawei U8650
	LG Optimus 2X
	LG Optimus L3
	Motorola Defy
	Motorola Milestone
	Nexus S
	Nokia Lumia 610
	Nokia Lumia 710
	Nokia Lumia 800
	Nokia Lumia 820
	Nokia Lumia 900
	Nokia Lumia 920
	Samsung Galaxy Ace
	Samsung Galaxy Ace 2
	Samsung Galaxy S2
	Sony Xperia E Dual
	BlackBerry Curve 9300*/

section#info {
    background: #f3f3f3;
}


.bizinfo {
    font-size: 100%;
}

p.infotext {
    margin: 2% 5% 5% 5%;
    font-size: 85%;
    text-indent: 10px;
    color: #444;
}


footer {
    min-height: 100px;
    border-bottom: 1px solid #ccc;
    background: #eee;
    color: #444;
    text-shadow: 0 1px 1px #f6f6f6;
    background-image: -webkit-gradient(linear,left top,left bottom,from(#fdfdfd),to(#eee));
    background-image: -webkit-linear-gradient(#fdfdfd,#eee);
    background-image: -moz-linear-gradient(#fdfdfd,#eee);
    background-image: -ms-linear-gradient(#fdfdfd,#eee);
    background-image: -o-linear-gradient(#fdfdfd,#eee);
    background-image: linear-gradient(#fdfdfd,#eee);
    border-top: 1px solid #538db9;
}

.footerlinks {
    width: 100%;
    border-bottom: 1px solid #538db9;
    padding: 1% 0 5% 0;
}

.footerlinks ul {   
   line-height: 27px;
   overflow: hidden;
   padding: 2.5% 0 0 3.5%;
   list-style: none;
   font-size: 3em;
}

.footerlinks ul li {
    display: inline;
    border: none;
}

.footerlinks ul li a {
    color: #1394ca;
    text-decoration: none;
    font-size: 37%;
    font-weight: 700;
}

.footerlinks li::after {
    content: "|";
    font-size: 35%;
    padding-left: 10px;
}

.first.last::after, .last.privacy::after {
    content: "";
}

.container {
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
    margin-bottom: 20px;
}

.partner {
    padding-top: 10px;
    margin-bottom: 10px;
}

.list-inline, .social-inline  {
    margin: 0;
    padding: 0;
}

.list-inline li {
    display: inline-block;
    background: none;
    border: none;
    width: inherit;
    margin: 0;
}

.container ul li:hover {
    border: none;
    width: inherit;
}

.payment.links .partner li img {
    width: 40px;
    height: 25px;
}

.socialcontainer {
    margin-right: auto;
    margin-left: auto;
    padding: 10px 15px 0 0;
}

.social-inline li {
    display: inline-block;
    background: none;
    border: none;
    width: inherit;
    margin: 1% 2% 0 0;
}

.bottombackground {
    margin: 0;
    padding: 0;
    width: 100%;
    font-size: 100%;
}

.bottomcontainer {
    height: 10%;
    border:1px solid #145072;
    background:#2567ab;
    background-image:-webkit-gradient(linear,left top,left bottom,from(#5f9cc5),to(#396b9e));
    background-image:-webkit-linear-gradient(#5f9cc5,#396b9e);
    background-image:-moz-linear-gradient(#5f9cc5,#396b9e);
    background-image:-ms-linear-gradient(#5f9cc5,#396b9e);
    background-image:-o-linear-gradient(#5f9cc5,#396b9e);
    background-image:linear-gradient(#5f9cc5,#396b9e);
}

p.copyright {
    padding: 5% 0 5% 0;
    text-align: center;
    font-size: 80%;
    text-shadow: none;
    color: #fff;
}
}

/* Additional Styles (Landscape) */

@media only screen and (max-width: 600px) and (orientation : landscape) {
	/* LANDSCAPE:
	Motorola Droid3
	Nexus 4
	Motorola Razr HD 4G
	Motorola Razr M 4G
	Motorola Defy
	Motorola Milestone
	Apple iPhone 5
	HP Veer*/  

section#info {
    background: #f3f3f3;
}


.bizinfo {
    font-size: 100%;
}

p.infotext {
    margin: 2% 5% 5% 5%;
    font-size: 85%;
    text-indent: 10px;
    color: #444;
}


footer {
    min-height: 100px;
    border-bottom: 1px solid #ccc;
    background: #eee;
    color: #444;
    text-shadow: 0 1px 1px #f6f6f6;
    background-image: -webkit-gradient(linear,left top,left bottom,from(#fdfdfd),to(#eee));
    background-image: -webkit-linear-gradient(#fdfdfd,#eee);
    background-image: -moz-linear-gradient(#fdfdfd,#eee);
    background-image: -ms-linear-gradient(#fdfdfd,#eee);
    background-image: -o-linear-gradient(#fdfdfd,#eee);
    background-image: linear-gradient(#fdfdfd,#eee);
    border-top: 1px solid #538db9;
}

.footerlinks {
    width: 100%;
    border-bottom: 1px solid #538db9;
    padding: 1% 0;
}

.footerlinks ul {   
   line-height: 27px;
   overflow: hidden;
   padding: 2.5%;
   list-style: none;
   display: inline;
}

.footerlinks ul li {
    display: inline;
    border: none;
    margin: 0 2% 0 0;
}

.footerlinks ul li a {
    color: #1394ca;
    text-decoration: none;
    font-size: 80%;
    font-weight: 700;
}

.footerlinks li::after {
    content: "|";
    font-size: 65%;
    padding-left: 10px;
}

.first.last::after {
    content: "";
}

.container {
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
    margin-bottom: 20px;
}

.partner {
    padding-top: 10px;
    margin-bottom: 10px;
}

.list-inline, .social-inline  {
    margin: 0;
    padding: 0;
}

.list-inline li {
    display: inline-block;
    background: none;
    border: none;
    width: inherit;
    margin: 0;
}

.container ul li:hover {
    border: none;
    width: inherit;
}

.payment.links .partner li img {
    width: 40px;
    height: 25px;
}

.socialcontainer {
    margin-right: auto;
    margin-left: auto;
    padding: 10px 15px 0 0;
}

.social-inline li {
    display: inline-block;
    background: none;
    border: none;
    width: inherit;
    margin: 1% 2% 0 0;
}

.bottombackground {
    margin: 0;
    padding: 0;
    width: 100%;
    font-size: 100%;
}

.bottomcontainer {
    height: 10%;
    border:1px solid #145072;
    background:#2567ab;
    background-image:-webkit-gradient(linear,left top,left bottom,from(#5f9cc5),to(#396b9e));
    background-image:-webkit-linear-gradient(#5f9cc5,#396b9e);
    background-image:-moz-linear-gradient(#5f9cc5,#396b9e);
    background-image:-ms-linear-gradient(#5f9cc5,#396b9e);
    background-image:-o-linear-gradient(#5f9cc5,#396b9e);
    background-image:linear-gradient(#5f9cc5,#396b9e);
}

p.copyright {
    padding: 5% 0 5% 0;
    text-align: center;
    font-size: 80%;
    text-shadow: none;
    color: #fff;
}
}

/* Additional Styles (Portrait) */

@media only screen and (max-width: 400px) and (orientation : portrait) {
	/* PORTRAIT:
	Samsung Galaxy Note
	Nexus 4
	HTC One S
	HTC One XL
	HTC Sensation XL
	HTC Velocity 4G
	Motorola Droid3
	Motorola Droid Razr
	Motorola Razr HD 4G
	Motorola Razr M 4G
	Nokia 500
	Samsung Galaxy Note 2
	Samsung Galaxy S3
	BlackBerry Bold 9900
	BlackBerry 9520
	BlackBerry Z10*/


section#info {
    background: #f3f3f3;
}


.bizinfo {
    font-size: 100%;
}

p.infotext {
    margin: 2% 5% 5% 5%;
    font-size: 85%;
    text-indent: 10px;
    color: #444;
}


footer {
    min-height: 100px;
    border-bottom: 1px solid #ccc;
    background: #eee;
    color: #444;
    text-shadow: 0 1px 1px #f6f6f6;
    background-image: -webkit-gradient(linear,left top,left bottom,from(#fdfdfd),to(#eee));
    background-image: -webkit-linear-gradient(#fdfdfd,#eee);
    background-image: -moz-linear-gradient(#fdfdfd,#eee);
    background-image: -ms-linear-gradient(#fdfdfd,#eee);
    background-image: -o-linear-gradient(#fdfdfd,#eee);
    background-image: linear-gradient(#fdfdfd,#eee);
    border-top: 1px solid #538db9;
}

.footerlinks {
    width: 100%;
    border-bottom: 1px solid #538db9;
    padding: 1% 0 5% 0;
}

.footerlinks ul {   
   line-height: 27px;
   overflow: hidden;
   padding: 2.5% 0 0 3.5%;
   list-style: none;
   font-size: 3em;
}

.footerlinks ul li {
    display: inline;
    border: none;
}

.footerlinks ul li a {
    color: #1394ca;
    text-decoration: none;
    font-size: 37%;
    font-weight: 700;
}

.footerlinks li::after {
    content: "|";
    font-size: 35%;
    padding-left: 10px;
}

.first.last::after, .last.privacy::after {
    content: "";
}

.container {
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
    margin-bottom: 20px;
}

.partner {
    padding-top: 10px;
    margin-bottom: 10px;
}

.list-inline, .social-inline  {
    margin: 0;
    padding: 0;
}

.list-inline li {
    display: inline-block;
    background: none;
    border: none;
    width: inherit;
    margin: 0;
}

.container ul li:hover {
    border: none;
    width: inherit;
}

.payment.links .partner li img {
    width: 40px;
    height: 25px;
}

.socialcontainer {
    margin-right: auto;
    margin-left: auto;
    padding: 10px 15px 0 0;
}

.social-inline li {
    display: inline-block;
    background: none;
    border: none;
    width: inherit;
    margin: 1% 2% 0 0;
}

.bottombackground {
    margin: 0;
    padding: 0;
    width: 100%;
    font-size: 100%;
}

.bottomcontainer {
    height: 10%;
    border:1px solid #145072;
    background:#2567ab;
    background-image:-webkit-gradient(linear,left top,left bottom,from(#5f9cc5),to(#396b9e));
    background-image:-webkit-linear-gradient(#5f9cc5,#396b9e);
    background-image:-moz-linear-gradient(#5f9cc5,#396b9e);
    background-image:-ms-linear-gradient(#5f9cc5,#396b9e);
    background-image:-o-linear-gradient(#5f9cc5,#396b9e);
    background-image:linear-gradient(#5f9cc5,#396b9e);
}

p.copyright {
    padding: 5% 0 5% 0;
    text-align: center;
    font-size: 80%;
    text-shadow: none;
    color: #fff;
}
}

/* Additional Styles (Landscape) */

@media only screen and (max-width: 640px) and (orientation : landscape) {
	/* LANDSCAPE:
	Samsung Galaxy Note
	HTC One S
	HTC One XL
	HTC Sensation XL
	HTC Velocity 4G
	Motorola Droid Razr
	Nokia 500
	Samsung Galaxy Note 2
	Samsung Galaxy S3*/



section#info {
    background: #f3f3f3;
}


.bizinfo {
    font-size: 100%;
}

p.infotext {
    margin: 2% 5% 5% 5%;
    font-size: 85%;
    text-indent: 10px;
    color: #444;
}


footer {
    min-height: 100px;
    border-bottom: 1px solid #ccc;
    background: #eee;
    color: #444;
    text-shadow: 0 1px 1px #f6f6f6;
    background-image: -webkit-gradient(linear,left top,left bottom,from(#fdfdfd),to(#eee));
    background-image: -webkit-linear-gradient(#fdfdfd,#eee);
    background-image: -moz-linear-gradient(#fdfdfd,#eee);
    background-image: -ms-linear-gradient(#fdfdfd,#eee);
    background-image: -o-linear-gradient(#fdfdfd,#eee);
    background-image: linear-gradient(#fdfdfd,#eee);
    border-top: 1px solid #538db9;
}

.footerlinks {
    width: 100%;
    border-bottom: 1px solid #538db9;
    padding: 1% 0;
}

.footerlinks ul {   
   line-height: 27px;
   overflow: hidden;
   padding: 2.5%;
   list-style: none;
   display: inline;
}

.footerlinks ul li {
    display: inline;
    border: none;
    margin: 0 2% 0 0;
}

.footerlinks ul li a {
    color: #1394ca;
    text-decoration: none;
    font-size: 80%;
    font-weight: 700;
}

.footerlinks li::after {
    content: "|";
    font-size: 65%;
    padding-left: 10px;
}

.container {
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
    margin-bottom: 20px;
}

.partner {
    padding-top: 10px;
    margin-bottom: 10px;
}

.list-inline, .social-inline  {
    margin: 0;
    padding: 0;
}

.list-inline li {
    display: inline-block;
    background: none;
    border: none;
    width: inherit;
    margin: 0;
}

.container ul li:hover {
    border: none;
    width: inherit;
}

.payment.links .partner li img {
    width: 40px;
    height: 25px;
}

.socialcontainer {
    margin-right: auto;
    margin-left: auto;
    padding: 10px 15px 0 0;
}

.social-inline li {
    display: inline-block;
    background: none;
    border: none;
    width: inherit;
    margin: 1% 2% 0 0;
}

.bottombackground {
    margin: 0;
    padding: 0;
    width: 100%;
    font-size: 100%;
}

.bottomcontainer {
    height: 10%;
    border:1px solid #145072;
    background:#2567ab;
    background-image:-webkit-gradient(linear,left top,left bottom,from(#5f9cc5),to(#396b9e));
    background-image:-webkit-linear-gradient(#5f9cc5,#396b9e);
    background-image:-moz-linear-gradient(#5f9cc5,#396b9e);
    background-image:-ms-linear-gradient(#5f9cc5,#396b9e);
    background-image:-o-linear-gradient(#5f9cc5,#396b9e);
    background-image:linear-gradient(#5f9cc5,#396b9e);
}

p.copyright {
    padding: 5% 0 5% 0;
    text-align: center;
    font-size: 80%;
    text-shadow: none;
    color: #fff;
}
}

@media only screen and (min-device-width : 768px) and (max-device-width : 1024px) and (-webkit-min-device-pixel-ratio: 1){
/* Apple Ipad */

section#info {
    background: #f3f3f3;
}


.bizinfo {
    font-size: 100%;
}

p.infotext {
    margin: 2% 5% 5% 5%;
    font-size: 85%;
    text-indent: 10px;
    color: #444;
}


footer {
    min-height: 100px;
    border-bottom: 1px solid #ccc;
    background: #eee;
    color: #444;
    text-shadow: 0 1px 1px #f6f6f6;
    background-image: -webkit-gradient(linear,left top,left bottom,from(#fdfdfd),to(#eee));
    background-image: -webkit-linear-gradient(#fdfdfd,#eee);
    background-image: -moz-linear-gradient(#fdfdfd,#eee);
    background-image: -ms-linear-gradient(#fdfdfd,#eee);
    background-image: -o-linear-gradient(#fdfdfd,#eee);
    background-image: linear-gradient(#fdfdfd,#eee);
    border-top: 1px solid #538db9;
}

.footerlinks {
    width: 100%;
    border-bottom: 1px solid #538db9;
    padding: 1% 0;
}

.footerlinks ul {   
   line-height: 27px;
   overflow: hidden;
   padding: 2.5%;
   list-style: none;
   display: inline;
}

.footerlinks ul li {
    display: inline;
    border: none;
    margin: 0 2% 0 0;
}

.footerlinks ul li a {
    color: #1394ca;
    text-decoration: none;
    font-size: 80%;
    font-weight: 700;
}

.footerlinks li::after {
    content: "|";
    font-size: 65%;
    padding-left: 10px;
}

.container {
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
    margin-bottom: 20px;
}

.partner {
    padding-top: 10px;
    margin-bottom: 10px;
}

.list-inline, .social-inline  {
    margin: 0;
    padding: 0;
}

.list-inline li {
    display: inline-block;
    background: none;
    border: none;
    width: inherit;
    margin: 0;
}

.container ul li:hover {
    border: none;
    width: inherit;
}

.payment.links .partner li img {
    width: 40px;
    height: 25px;
}

.socialcontainer {
    margin-right: auto;
    margin-left: auto;
    padding: 10px 15px 0 0;
}

.social-inline li {
    display: inline-block;
    background: none;
    border: none;
    width: inherit;
    margin: 1% 2% 0 0;
}

.bottombackground {
    margin: 0;
    padding: 0;
    width: 100%;
    font-size: 100%;
}

.bottomcontainer {
    height: 10%;
    border:1px solid #145072;
    background:#2567ab;
    background-image:-webkit-gradient(linear,left top,left bottom,from(#5f9cc5),to(#396b9e));
    background-image:-webkit-linear-gradient(#5f9cc5,#396b9e);
    background-image:-moz-linear-gradient(#5f9cc5,#396b9e);
    background-image:-ms-linear-gradient(#5f9cc5,#396b9e);
    background-image:-o-linear-gradient(#5f9cc5,#396b9e);
    background-image:linear-gradient(#5f9cc5,#396b9e);
}

p.copyright {
    padding: 5% 0 5% 0;
    text-align: center;
    font-size: 80%;
    text-shadow: none;
    color: #fff;
}
}

@media only screen and (max-width: 540px) and (orientation : landscape) {
    /* LANDSCAPE:
	HTC Desire
	HTC Desire HD
	HTC One V
	LG Optimus 2X
	Nexus S
	Samsung Galaxy Ace 2
	Samsung Galaxy S2
	BlackBerry Z10*/

section#info {
    background: #f3f3f3;
}


.bizinfo {
    font-size: 100%;
}

p.infotext {
    margin: 2% 5% 5% 5%;
    font-size: 85%;
    text-indent: 10px;
    color: #444;
}


footer {
    min-height: 100px;
    border-bottom: 1px solid #ccc;
    background: #eee;
    color: #444;
    text-shadow: 0 1px 1px #f6f6f6;
    background-image: -webkit-gradient(linear,left top,left bottom,from(#fdfdfd),to(#eee));
    background-image: -webkit-linear-gradient(#fdfdfd,#eee);
    background-image: -moz-linear-gradient(#fdfdfd,#eee);
    background-image: -ms-linear-gradient(#fdfdfd,#eee);
    background-image: -o-linear-gradient(#fdfdfd,#eee);
    background-image: linear-gradient(#fdfdfd,#eee);
    border-top: 1px solid #538db9;
}

.footerlinks {
    width: 100%;
    border-bottom: 1px solid #538db9;
    padding: 1% 0;
}

.footerlinks ul {   
   line-height: 27px;
   overflow: hidden;
   padding: 2.5%;
   list-style: none;
   display: inline;
}

.footerlinks ul li {
    display: inline;
    border: none;
    margin: 0 2% 0 0;
}

.footerlinks ul li a {
    color: #1394ca;
    text-decoration: none;
    font-size: 80%;
    font-weight: 700;
}

.footerlinks li::after {
    content: "|";
    font-size: 65%;
    padding-left: 10px;
}

.container {
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
    margin-bottom: 20px;
}

.partner {
    padding-top: 10px;
    margin-bottom: 10px;
}

.list-inline, .social-inline  {
    margin: 0;
    padding: 0;
}

.list-inline li {
    display: inline-block;
    background: none;
    border: none;
    width: inherit;
    margin: 0;
}

.container ul li:hover {
    border: none;
    width: inherit;
}

.payment.links .partner li img {
    width: 40px;
    height: 25px;
}

.socialcontainer {
    margin-right: auto;
    margin-left: auto;
    padding: 10px 15px 0 0;
}

.social-inline li {
    display: inline-block;
    background: none;
    border: none;
    width: inherit;
    margin: 1% 2% 0 0;
}

.bottombackground {
    margin: 0;
    padding: 0;
    width: 100%;
    font-size: 100%;
}

.bottomcontainer {
    height: 10%;
    border:1px solid #145072;
    background:#2567ab;
    background-image:-webkit-gradient(linear,left top,left bottom,from(#5f9cc5),to(#396b9e));
    background-image:-webkit-linear-gradient(#5f9cc5,#396b9e);
    background-image:-moz-linear-gradient(#5f9cc5,#396b9e);
    background-image:-ms-linear-gradient(#5f9cc5,#396b9e);
    background-image:-o-linear-gradient(#5f9cc5,#396b9e);
    background-image:linear-gradient(#5f9cc5,#396b9e);
}

p.copyright {
    padding: 5% 0 5% 0;
    text-align: center;
    font-size: 80%;
    text-shadow: none;
    color: #fff;
}
}
/* Basic Footer Styles */

section#info {
    background: #f3f3f3;
}


.bizinfo {
    font-size: 100%;
}

p.infotext {
    margin: 2% 5% 5% 5%;
    font-size: 85%;
    text-indent: 10px;
    color: #444;
}


footer {
    min-height: 100px;
    border-bottom: 1px solid #ccc;
    background: #eee;
    color: #444;
    text-shadow: 0 1px 1px #f6f6f6;
    background-image: -webkit-gradient(linear,left top,left bottom,from(#fdfdfd),to(#eee));
    background-image: -webkit-linear-gradient(#fdfdfd,#eee);
    background-image: -moz-linear-gradient(#fdfdfd,#eee);
    background-image: -ms-linear-gradient(#fdfdfd,#eee);
    background-image: -o-linear-gradient(#fdfdfd,#eee);
    background-image: linear-gradient(#fdfdfd,#eee);
    border-top: 1px solid #538db9;
}

.footerlinks {
    width: 100%;
    border-bottom: 1px solid #538db9;
    padding: 1% 0 5% 0;
}

.footerlinks ul {   
   line-height: 27px;
   overflow: hidden;
   padding: 2.5% 0 0 3.5%;
   list-style: none;
   font-size: 3em;
}

.footerlinks ul li {
    display: inline;
    border: none;
}

.footerlinks ul li a {
    color: #1394ca;
    text-decoration: none;
    font-size: 37%;
    font-weight: 700;
}

.footerlinks li::after {
    content: "|";
    font-size: 35%;
    padding-left: 10px;
}

.first.last::after, .last.privacy::after {
    content: "";
}

.container {
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
    margin-bottom: 20px;
}

.partner {
    padding-top: 10px;
    margin-bottom: 10px;
}

.list-inline, .social-inline  {
    margin: 0;
    padding: 0;
}

.list-inline li {
    display: inline-block;
    background: none;
    border: none;
    width: inherit;
    margin: 0;
}

.container ul li:hover {
    border: none;
    width: inherit;
}

.payment.links .partner li img {
    width: 40px;
    height: 25px;
}

.socialcontainer {
    margin-right: auto;
    margin-left: auto;
    padding: 10px 15px 0 0;
}

.social-inline li {
    display: inline-block;
    background: none;
    border: none;
    width: inherit;
    margin: 1% 2% 0 0;
}

.bottombackground {
    margin: 0;
    padding: 0;
    width: 100%;
    font-size: 100%;
}

.bottomcontainer {
    height: 10%;
    border:1px solid #145072;
    background:#2567ab;
    background-image:-webkit-gradient(linear,left top,left bottom,from(#5f9cc5),to(#396b9e));
    background-image:-webkit-linear-gradient(#5f9cc5,#396b9e);
    background-image:-moz-linear-gradient(#5f9cc5,#396b9e);
    background-image:-ms-linear-gradient(#5f9cc5,#396b9e);
    background-image:-o-linear-gradient(#5f9cc5,#396b9e);
    background-image:linear-gradient(#5f9cc5,#396b9e);
}

p.copyright {
    padding: 5% 0 5% 0;
    text-align: center;
    font-size: 80%;
    text-shadow: none;
    color: #fff;
}

