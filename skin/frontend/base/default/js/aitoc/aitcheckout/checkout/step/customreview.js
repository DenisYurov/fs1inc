
/**
 * All-In-One Checkout : All-In-One Checkout v1.1.0 (OPCB Unit)
 *
 * @package:     Aitoc_Aitcheckout / Aitoc_Aitcheckout
 * @version      1.4.17 - 1.4.17
 * @license:     FoOiFpEBsy1CQtPSBPTqBFwAOZTnyw3qCMIlTMg96m
 * @copyright:   Copyright (c) 2017 AITOC, Inc. (http://www.aitoc.com)
 */
var AitCustomreview = Class.create(Step,
    {
        afterSet: function()
        {
            if(aitCheckout.isStatusChanged()) {
                aitCheckout.getStep('customreview').onUpdateResponseAfter({customreview:{length:0}})
            }
        }
    });