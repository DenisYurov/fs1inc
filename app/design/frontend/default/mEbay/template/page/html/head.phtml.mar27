<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.magentocommerce.com for more information.
 *
 * @category    design
 * @package     base_default
 * @copyright   Copyright (c) 2011 Magento Inc. (https://www.magentocommerce.com)
 * @license     https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */
?>
<meta http-equiv="Content-Type" content="<?php echo $this->getContentType() ?>" />
<meta name="viewport" content="width=device-width, initial-scale=0">
<title><?php echo $this->getTitle() ?></title>
<meta name="description" content="<?php echo htmlspecialchars($this->getDescription()) ?>" />
<meta name="keywords" content="<?php echo htmlspecialchars($this->getKeywords()) ?>" />
<meta name="robots" content="<?php echo htmlspecialchars($this->getRobots()) ?>" />
<link rel="icon" href="<?php echo $this->getFaviconFile(); ?>" type="image/x-icon" />
<link rel="shortcut icon" href="<?php echo $this->getFaviconFile(); ?>" type="image/x-icon" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!--[if lt IE 7]>
<script type="text/javascript">
//<![CDATA[
    var BLANK_URL = '<?php echo $this->helper('core/js')->getJsUrl('blank.html') ?>';
    var BLANK_IMG = '<?php echo $this->helper('core/js')->getJsUrl('spacer.gif') ?>';
//]]>
</script>
<![endif]-->

<!-- Google Tag Manager -->
<?php
 
if(Mage::app()->getRequest()->getRouteName() == 'cms' && Mage::getSingleton('cms/page')->getIdentifier() == 'home'):
$products = Mage::getModel('catalog/product')
->getCollection()
->addAttributeToSort('created_at', 'DESC')
->addAttributeToSelect('*')
->addAttributeToFilter('featured', array('yes' => true))
->load();
$productIds = array();
$count=0;
$$hometotal = 0;
foreach ($products as $product):
    $productIds[] = $product->getSku();
$hometotal += number_format($product->getPrice(), 2, '.', '');
$count++;
if($count==12):
    break;
endif;
endforeach;
?>

<script>
dataLayer = [{
<?php if(count($productIds)==1): ?>
'ecomm_prodid': '<?php echo implode(',', $productIds) ?>',       
<?php else: ?>
'ecomm_prodid': ['<?php echo implode("','", $productIds) ?>'],
<?php endif; ?>
  'ecomm_pagetype': 'home',
  'ecomm_totalvalue': <?php echo number_format($hometotal, 2, '.', '') ?>
}]; 
</script>
<?php endif; ?>

<?php

//if(Mage::registry('current_category')):
if(Mage::app()->getFrontController()->getRequest()->getControllerName()=='category'):
$categoryTotal=0;
$productIds = array();
$toolbar = new Mage_Catalog_Block_Product_List_Toolbar();
$list = new Mage_Catalog_Block_Product_List();
$toolbar->setCollection($list->getLoadedProductCollection());
foreach ($list->getLoadedProductCollection() as $product):
    $productIds[] = Mage::getModel('catalog/product')->load($product->getId())->getSku();
    $categoryTotal += number_format($product->getPrice(), 2, '.', '');
endforeach;
?>
<script>
dataLayer = [{
<?php if(count($productIds)==1): ?>
'ecomm_prodid': '<?php echo implode(',', $productIds) ?>',       
<?php else: ?>
'ecomm_prodid': ['<?php echo implode("','", $productIds) ?>'],
<?php endif; ?>
  'ecomm_pagetype': 'category',
  'ecomm_totalvalue': <?php echo number_format($categoryTotal, 2, '.', '') ?>
}]; 
</script>
<?php endif;  ?>

<?php
if (Mage::app()->getFrontController()->getAction()->getFullActionName() == 'checkout_cart_index' || Mage::getURL('aitcheckout/checkout') == Mage::helper('core/url')->getCurrentUrl()):
    $cart = Mage::getModel('checkout/cart')->getQuote();
    $productIds = array();
    foreach ($cart->getAllItems() as $item):
        $productIds[] = $item->getProduct()->getSku();
    endforeach;
    if(count($productIds) >0 ):
        
?>
<script>
dataLayer = [{
<?php if(count($productIds)==1): ?>
'ecomm_prodid': '<?php echo implode(',', $productIds) ?>',       
<?php else: ?>
'ecomm_prodid': ['<?php echo implode('\',\'', $productIds) ?>'],
<?php endif; ?>
'ecomm_pagetype': 'cart',
'ecomm_totalvalue': <?php echo number_format($cart->getGrandTotal(), 2, '.', '') ?> 
}]; 
</script>
    <?php
    endif;
endif;

if($_product = Mage::registry('current_product')): ?>
<script>
dataLayer =[{
  'ecomm_prodid': '<?php echo $_product->getSku(); ?>',
  'ecomm_pagetype': 'product',
  'ecomm_totalvalue': <?php echo number_format($_product->getPrice(), 2, '.', ''); ?>
}];
</script>
<?php endif; ?>

<?php 
//checkout/onepage_success
if(Mage::app()->getRequest()->getActionName() == 'success'):
    $orderId = Mage::getSingleton('checkout/session')->getLastRealOrderId();
    $order = Mage::getModel('sales/order')->loadByIncrementId($orderId);
    $items = $order->getAllVisibleItems();
    $productIds = array();
    $products = array();
    $purchase = array();
    $purchase['id'] = $orderId;
    $purchase['affiliation'] = 'Flagship One, Inc';
    $purchase['revenue'] = number_format($order->getGrandTotal(), 2, '.', '');
    $purchase['tax'] = number_format($order->getTaxAmount(), 2, '.', '');
    $purchase['shipping'] = number_format($order->getShippingAmount(), 2, '.', '');
    $purchase['coupon'] = $order->getCouponCode();
    $products_string = "";
    foreach( $items as $item ):
        $productIds[] = $item->getSku();
        $products['name'] = $item->getName();
        $products['id'] = $item->getSku();
        $products['price'] = number_format($item->getPrice(), 2, '.', '');
        $products['quantity'] = (int)$item->getQtyOrdered();
        $_product =Mage::getModel('catalog/product')->load($item->getProductId());
        $make  = $_product->getAttributeText('make');
        $model = $_product->getAttributeText('model');
        $year = $_product->getAttributeText('year');
        $enginesize = $_product->getAttributeText('enginesize');
        
        $category = array();
        if(!empty(trim($make." ".$model))):
            if(gettype($model)=='string'):
                $category[] = trim($make." ".$model);
            elseif(gettype($model)=='array'):
                if(count($model)>0):
                    $category[] = trim($make." ".implode(" ", $model));
                endif;
            endif;
        endif;
        if(!empty($year)):
            if(gettype($year)=='string'):
                $category[] = trim($year);
            elseif(gettype($year)=='array'):
                if(count($year)>0):
                    $category[] = implode(" ", $year);
                endif;
            endif;
        endif;
        if(!empty($enginesize)):
            if(gettype($enginesize)=='string'):
                $category[] = trim($enginesize);
            elseif(gettype($enginesize)=='array'):
                if(count($enginesize)>0):
                    $category[] = implode(" ", $enginesize);
                endif;
            endif;
        endif;
        $products['category'] = implode("/", $category);
        $products['brand'] = trim($make );
        $products['variant'] = $_product->getHardware();
        
        $products_string[] = "{'name':'".$products['name']."','id':'".$products['id']."','price':'".$products['price']."','quantity':'".$products['quantity']."','category':'".$products['category']."','brand':'".$products['brand']."','variant':'".$products['variant']."'}";
    endforeach;
    
?>
<script>
dataLayer = [{
    <?php if(count($productIds)==1): ?>
    'ecomm_prodid': '<?php echo implode(',', $productIds) ?>',       
    <?php else: ?>
    'ecomm_prodid': ['<?php echo implode("','", $productIds) ?>'],
    <?php endif; ?>
    'ecomm_pagetype': 'purchase',
    'ecomm_totalvalue': '<?php echo number_format($order->getGrandTotal(), 2, '.', '') ?>',
    'ecommerce': {
        'purchase': {
          'actionField': {
            'id': '<?= $purchase['id'] ?>',
            'affiliation': '<?= $purchase['affiliation'] ?>',
            'revenue': '<?= $purchase['revenue'] ?>',
            'tax':'<?= $purchase['tax'] ?>',
            'shipping': '<?= $purchase['shipping'] ?>',
            'coupon': '<?= $purchase['coupon'] ?>'
          },
          'products': [<?php echo join(",", $products_string) ?>]
        }
    }
}]; 
</script>
    <?php
endif;
?>

<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TFX4RGS');
</script>

<!-- End Google Tag Manager -->

<?php echo $this->getCssJsHtml() ?>
<?php echo $this->getChildHtml() ?>
<?php echo $this->helper('core/js')->getTranslatorScript() ?>
<?php echo $this->getIncludes() ?>

