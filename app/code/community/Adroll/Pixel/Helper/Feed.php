<?php
class Adroll_Pixel_Helper_Feed extends Mage_Core_Helper_Abstract
{
    const FEED_PAGE_SIZE = 100;
    private $baseCurrencyCode;

    public function __construct()
    {
        $this->baseCurrencyCode = Mage::app()->getStore()->getBaseCurrencyCode();
    }

    private function convertCurrency($value, $currencyCode)
    {
        return Mage::helper('directory')->currencyConvert($value, $this->baseCurrencyCode, $currencyCode);
    }

    private function serializeProduct($productId, $currencyCode, $storeId)
    {
        $product = Mage::getModel('catalog/product')->setStoreId($storeId)->load($productId);
        $manufacturer = $product->getAttributeText('manufacturer');

        return array(
            'id' => $productId,
            'title' => $product->getName(),
            'description' => $product->getData('description'),
            'price' => $this->convertCurrency($product->getPrice(), $currencyCode),
            'sale_price' => $this->convertCurrency($product->getFinalPrice(), $currencyCode),
            'url' => $product->getProductUrl(),
            'brand' => $manufacturer === false ? null : $manufacturer,
            'image_url' => $product->getImageUrl()
        );
    }

    public function getFeedableProducts($store)
    {
        $productCollection = Mage::getResourceModel('catalog/product_collection')
            // Exclude disabled products
            ->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))
            // Exclude products that are not visible individually
            ->addAttributeToFilter('visibility', array('neq' => Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE))
            // Exclude products not available in this store
            ->addStoreFilter($store);

        // Exclude out-of-stock products
        Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($productCollection);

        return $productCollection;
    }

    public function generateProductFeed($currencyCode, $storeCode, $page)
    {
        $productFeed = array('products' => array());
        $store = Mage::getModel('core/store')->load($storeCode, 'code'); //->getId();

        $productCollection = $this->getFeedableProducts($store);

        // Only add products to feed result if the requested page exists. We have to do this manually because the
        // setCurPage function still returns results when you go past the last page.
        $lastPage = ceil($productCollection->getSize() / self::FEED_PAGE_SIZE);
        if ($page <= $lastPage) {
            $productCollection->setPageSize(self::FEED_PAGE_SIZE)->setCurPage($page);
            foreach ($productCollection as $product) {
                $productFeed['products'][] = $this->serializeProduct($product->getId(), $currencyCode, $store->getId());
            }
        }

        return $productFeed;
    }
}
