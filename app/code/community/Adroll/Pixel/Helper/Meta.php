<?php
class Adroll_Pixel_Helper_Meta extends Mage_Core_Helper_Abstract
{
    private $defaultCurrencies;

    public function __construct()
    {
        $this->defaultCurrencies = $this->getCurrenciesForStore(0);
    }

    public function serializeStore($store)
    {
        return array(
            'id' => $store->getId(),
            'name' => $store->getName(),
            'code' => $store->getCode(),
            'currencies' => $this->getCurrenciesForStore($store->getId()),
            'locale' => $this->getLocaleForStore($store->getId()),
            'product_count' => Mage::helper('adroll_pixel/feed')->getFeedableProducts($store)->count()
        );
    }

    public function getCurrenciesForStore($storeId)
    {
        $rows = Mage::getModel('core/config_data')
            ->getCollection()
            ->addFieldToFilter('path', 'currency/options/allow')
            ->addFieldToFilter('scope_id', $storeId)
            ->getData();

        if (count($rows) > 0) {
            return explode(',', $rows[0]['value']);
        } else {
            return $this->defaultCurrencies;
        }
    }

    public function getLocaleForStore($storeId)
    {
        return Mage::getStoreConfig('general/locale/code', $storeId);
    }

    public function generateStoreGroupMeta($storeGroupId)
    {
        $configHelper = Mage::helper('adroll_pixel/config');
        $storeGroupMeta = array(
            'advertisable_eid' => $configHelper->getAdvertisableEid($storeGroupId),
            'pixel_eid' => $configHelper->getPixelEid($storeGroupId),
            'stores' => array()
        );
        $stores = Mage::getModel('core/store')->getCollection()->addFieldToFilter('group_id', $storeGroupId);
        foreach ($stores as $store) {
            $storeGroupMeta['stores'][] = $this->serializeStore($store);
        }
        return $storeGroupMeta;
    }
}
