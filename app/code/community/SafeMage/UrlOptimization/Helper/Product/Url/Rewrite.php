<?php
/*
NOTICE OF LICENSE

This source file is subject to the SafeMageEULA that is bundled with this package in the file LICENSE.txt.

It is also available at this URL: https://www.safemage.com/LICENSE_EULA.txt

Copyright (c)  SafeMage (https://www.safemage.com/)
*/


class SafeMage_UrlOptimization_Helper_Product_Url_Rewrite extends Mage_Catalog_Helper_Product_Url_Rewrite implements Mage_Catalog_Helper_Product_Url_Rewrite_Interface
{
    /**
     * Prepare and return select
     *
     * @param array $productIds
     * @param int $categoryId
     * @param int $storeId
     * @return Varien_Db_Select
     */
    public function getTableSelect(array $productIds, $categoryId, $storeId)
    {
        if (Mage::helper('safemage_urloptimization')->isGlobalUrlEnabled()) {
            $storeId = 0;
        }
        return parent::getTableSelect($productIds, $categoryId, $storeId);
    }

    /**
     * Prepare url rewrite left join statement for given select instance and store_id parameter.
     *
     * @param Varien_Db_Select $select
     * @param int $storeId
     * @return Mage_Catalog_Helper_Product_Url_Rewrite_Interface
     */
    public function joinTableToSelect(Varien_Db_Select $select, $storeId)
    {
        if (Mage::helper('safemage_urloptimization')->isGlobalUrlEnabled()) {
            $storeId = 0;
        }
        return parent::joinTableToSelect($select, $storeId);
    }
}
