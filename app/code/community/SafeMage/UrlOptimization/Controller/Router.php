<?php
/*
NOTICE OF LICENSE

This source file is subject to the SafeMageEULA that is bundled with this package in the file LICENSE.txt.

It is also available at this URL: https://www.safemage.com/LICENSE_EULA.txt

Copyright (c)  SafeMage (https://www.safemage.com/)
*/

class SafeMage_UrlOptimization_Controller_Router extends Mage_Core_Controller_Varien_Router_Abstract
{    
    public function initControllerRouters($observer)
    {
        $front  = $observer->getEvent()->getFront();
        $front->addRouter('safemage_urloptimization', $this);
    }
    
    public function match(Zend_Controller_Request_Http $request)
    {

        if (!Mage::helper('safemage_urloptimization')->isRoutingEnabled()) {
            return false;
        }

        $requestPath = ltrim($request->getPathInfo(), '/');

        if (preg_match('/^(.+)-[0-9]+(.html|.htm|\/)$/is', $requestPath, $matches)) {
            $requestPath = $matches[1] . $matches[2];
            $resourceModel = Mage::getResourceModel('safemage_urloptimization/catalog_url');
            $rewrite = $resourceModel->getRewriteByRequestPath($requestPath, Mage::app()->getStore()->getId());
            if ($rewrite) {
                Mage::app()->getResponse()->setRedirect($requestPath, 301)
                    ->sendResponse();

                $parts = explode('/', $rewrite->getIdPath());

                $controller = 'product';
                if ($parts[0] == 'category') {
                    $controller =  'category';
                }

                $entityId = 0;
                if (count($parts) > 1) {
                    $entityId = intval($parts[count($parts) - 1]);
                }

                $request->setModuleName('catalog')
                    ->setControllerName($controller)
                    ->setActionName('view')
                    ->setParam('id', $entityId);

                $request->setAlias(
                    Mage_Core_Model_Url_Rewrite::REWRITE_REQUEST_PATH_ALIAS,
                    $requestPath
                );

                return true;
            }
        }

        return false;
    }
}
