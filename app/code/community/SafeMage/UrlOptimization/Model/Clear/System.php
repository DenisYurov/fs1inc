<?php
/*
NOTICE OF LICENSE

This source file is subject to the SafeMageEULA that is bundled with this package in the file LICENSE.txt.

It is also available at this URL: https://www.safemage.com/LICENSE_EULA.txt

Copyright (c)  SafeMage (https://www.safemage.com/)
*/

class SafeMage_UrlOptimization_Model_Clear_System
{
    public function process()
    {
        $resource = Mage::getSingleton('core/resource');
        $connection = $resource->getConnection('core_write');

        $stmt = $connection->query("DELETE FROM `". $resource->getTableName('core/url_rewrite') ."`
            WHERE `is_system` = 1 AND (`product_id` > 0 OR `category_id` > 0);");
        $result = $stmt->rowCount();

        return $result;
    }
}
