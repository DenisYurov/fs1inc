<?php
/*
NOTICE OF LICENSE

This source file is subject to the SafeMageEULA that is bundled with this package in the file LICENSE.txt.

It is also available at this URL: https://www.safemage.com/LICENSE_EULA.txt

Copyright (c)  SafeMage (https://www.safemage.com/)
*/

class SafeMage_UrlOptimization_Model_System_Config_Backend_Global extends Mage_Core_Model_Config_Data
{
    protected function _afterSave()
    {
        parent::_afterSave();
        if ($this->isValueChanged()) {
            $resource = Mage::getSingleton('core/resource');
            $connection = $resource->getConnection('core_write');

            $select = $connection
                ->select()
                ->from($resource->getTableName('eav/attribute'), array('attribute_id'))
                ->where('attribute_code = ?', 'url_key');
            $rows = $connection->fetchAll($select);

            $attributeIds = array();
            foreach($rows as $row) {
                $attributeIds[] = $row['attribute_id'];
            }

            $connection->update(
                $resource->getTableName('catalog/eav_attribute'),
                array('is_global' => $this->getValue()),
                "`attribute_id` IN (" . implode(',', $attributeIds) . ")"
            );
        }
        return $this;
    }
}
