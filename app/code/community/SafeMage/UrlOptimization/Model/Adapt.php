<?php
/*
NOTICE OF LICENSE

This source file is subject to the SafeMageEULA that is bundled with this package in the file LICENSE.txt.

It is also available at this URL: https://www.safemage.com/LICENSE_EULA.txt

Copyright (c)  SafeMage (https://www.safemage.com/)
*/

class SafeMage_UrlOptimization_Model_Adapt
{
    public function process()
    {
        if (Mage::helper('safemage_urloptimization')->isGlobalUrlEnabled()) {
            $resource = Mage::getSingleton('core/resource');
            $connection = $resource->getConnection('core_write');

            $urlRewriteTable = $resource->getTableName('core/url_rewrite');

            $stmt = $connection->query("UPDATE IGNORE `". $urlRewriteTable ."`
                SET `store_id` = 0
                WHERE `is_system` = 0
                    AND `options` = 'RP'
                    AND (`product_id` > 0 OR `category_id` > 0)
                    AND `store_id` > 0;");
            $result = $stmt->rowCount();

            $connection->query("DELETE FROM `". $urlRewriteTable ."`
                WHERE `is_system` = 0
                    AND `options` = 'RP'
                    AND (`product_id` > 0 OR `category_id` > 0)
                    AND `store_id` > 0;");
        } else {
            $result = 0;
        }

        return $result;
    }
}
