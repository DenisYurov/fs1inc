<?php
/**
 *
 * Created by:  Milan Simek
 * Company:     Plugin Company
 *
 * LICENSE: http://plugin.company/docs/magento-extensions/magento-extension-license-agreement
 *
 * YOU WILL ALSO FIND A PDF COPY OF THE LICENSE IN THE DOWNLOADED ZIP FILE
 *
 * FOR QUESTIONS AND SUPPORT
 * PLEASE DON'T HESITATE TO CONTACT US AT:
 *
 * SUPPORT@PLUGIN.COMPANY
 *
 */
 ?>
<?php

/**
 * Contactforms module upgrade script
 *
 * @category    Plugincompany
 * @package     Plugincompany_Contactforms
 * @author      Milan Simek
 */
$this->startSetup();
$installer = $this;

$formTable = $installer->getTable('plugincompany_contactforms/form');

$installer->run("
ALTER TABLE `{$formTable}` ADD COLUMN `css_classes` text NULL COMMENT 'Form custom CSS classes';
ALTER TABLE `{$formTable}` ADD COLUMN `custom_css` text NULL COMMENT 'Form custom CSS';  
ALTER TABLE `{$formTable}` ADD COLUMN `pageload_js` text NULL COMMENT 'Pageload custom JS' AFTER `arbitrary_js`;
ALTER TABLE `{$formTable}` ADD COLUMN `beforesubmit_js` text NULL COMMENT 'Before form submit custom JS' AFTER `arbitrary_js`;
    ");

$installer->endSetup();