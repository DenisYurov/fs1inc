<?php
/**
 *
 * Created by:  Milan Simek
 * Company:     Plugin Company
 *
 * LICENSE: http://plugin.company/docs/magento-extensions/magento-extension-license-agreement
 *
 * YOU WILL ALSO FIND A PDF COPY OF THE LICENSE IN THE DOWNLOADED ZIP FILE
 *
 * FOR QUESTIONS AND SUPPORT
 * PLEASE DON'T HESITATE TO CONTACT US AT:
 *
 * SUPPORT@PLUGIN.COMPANY
 *
 */

/**
 * Contactforms module install script
 *
 * @category    Plugincompany
 * @package     Plugincompany_Contactforms
 * @author      Milan Simek
 */
$this->startSetup();

$conn = $this->getConnection();

$this->getConnection()
    ->addColumn($this->getTable('plugincompany_contactforms/form'), 'css_classes', array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable' => true,
        'comment' => 'Form custom CSS classes'
    ));

$this->getConnection()
    ->addColumn($this->getTable('plugincompany_contactforms/form'), 'custom_css', array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable' => true,
        'comment' => 'Form custom CSS'
    ));

$this->getConnection()
    ->addColumn($this->getTable('plugincompany_contactforms/form'), 'pageload_js', array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable' => true,
        'comment' => 'Pageload custom JS',
        'after' => 'arbitrary_js'
    ));

$this->getConnection()
    ->addColumn($this->getTable('plugincompany_contactforms/form'), 'beforesubmit_js', array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable' => true,
        'comment' => 'Before form submit custom JS',
        'after' => 'arbitrary_js'
    ));

$this->endSetup();
