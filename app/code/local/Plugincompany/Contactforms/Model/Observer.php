<?php
/**
 *
 * Created by:  Milan Simek
 * Company:     Plugin Company
 *
 * LICENSE: http://plugin.company/docs/magento-extensions/magento-extension-license-agreement
 *
 * YOU WILL ALSO FIND A PDF COPY OF THE LICENSE IN THE DOWNLOADED ZIP FILE
 *
 * FOR QUESTIONS AND SUPPORT
 * PLEASE DON'T HESITATE TO CONTACT US AT:
 *
 * SUPPORT@PLUGIN.COMPANY
 *
 */
class Plugincompany_Contactforms_Model_Observer {

    public function pcContactformsFormSubmitBefore(Varien_Event_Observer $observer){
//        $form = $observer->getEvent()->getForm();
//        $params = $observer->getEvent()->getParams();
//        $params->setStopProcessing(true);
    }

    public function plugincompanyContactformsEntrySaveBefore(Varien_Event_Observer $observer){
//        $entry = $observer->getEvent()->getEntry();
    }
}