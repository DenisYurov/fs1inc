<?php
/**
 * All-In-One Checkout : All-In-One Checkout v1.1.0 (OPCB Unit)
 *
 * @package:     Aitoc_Aitcheckout / Aitoc_Aitcheckout
 * @version      1.4.17 - 1.4.17
 * @license:     FoOiFpEBsy1CQtPSBPTqBFwAOZTnyw3qCMIlTMg96m
 * @copyright:   Copyright (c) 2017 AITOC, Inc. (http://www.aitoc.com)
 */
class Aitoc_Aitcheckout_Model_System_Config_Source_Design
{

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => Aitoc_Aitcheckout_Helper_Data::DESIGN_DEFAULT,    'label'=>Mage::helper('aitcheckout')->__('Default Design')),
            array('value' => Aitoc_Aitcheckout_Helper_Data::DESIGN_COMPACT,    'label'=>Mage::helper('aitcheckout')->__('Compact v1 Design')),
            array('value' => Aitoc_Aitcheckout_Helper_Data::DESIGN_COMPACT_V2, 'label'=>Mage::helper('aitcheckout')->__('Compact v2 Design')),
        );
    }

}