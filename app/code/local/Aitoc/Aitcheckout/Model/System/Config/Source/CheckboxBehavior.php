<?php
/**
 * All-In-One Checkout : All-In-One Checkout v1.1.0 (OPCB Unit)
 *
 * @package:     Aitoc_Aitcheckout / Aitoc_Aitcheckout
 * @version      1.4.17 - 1.4.17
 * @license:     FoOiFpEBsy1CQtPSBPTqBFwAOZTnyw3qCMIlTMg96m
 * @copyright:   Copyright (c) 2017 AITOC, Inc. (http://www.aitoc.com)
 */
class Aitoc_Aitcheckout_Model_System_Config_Source_CheckboxBehavior
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => Aitoc_Aitcheckout_Helper_Data::CHECKBOX_AVAILABLE_INSTANTLY,   'label'=>Mage::helper('aitcheckout')->__('Mark the checkbox')),
            array('value' => Aitoc_Aitcheckout_Helper_Data::CHECKBOX_VIEWING_REQUIRED,      'label'=>Mage::helper('aitcheckout')->__('Display a pop-up window')),
        );
    }

}