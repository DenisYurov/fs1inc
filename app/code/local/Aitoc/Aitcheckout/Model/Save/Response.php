<?php
/**
 * All-In-One Checkout : All-In-One Checkout v1.1.0 (OPCB Unit)
 *
 * @package:     Aitoc_Aitcheckout / Aitoc_Aitcheckout
 * @version      1.4.17 - 1.4.17
 * @license:     FoOiFpEBsy1CQtPSBPTqBFwAOZTnyw3qCMIlTMg96m
 * @copyright:   Copyright (c) 2017 AITOC, Inc. (http://www.aitoc.com)
 */
class Aitoc_Aitcheckout_Model_Save_Response
{
    
    protected $_data = array();
    
    public function addStepResponse($step, $response)
    {
        $this->_data[$step] = $response;
        return $this;    
    }   
    
    public function isValid()
    {
        return true;    
    } 
    
    public function toArray()
    {
        if ($this->isValid())
        {
            return $this->_data;
        }        
    } 
    
}