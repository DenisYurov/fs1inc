<?php
/**
 * All-In-One Checkout : All-In-One Checkout v1.1.0 (OPCB Unit)
 *
 * @package:     Aitoc_Aitcheckout / Aitoc_Aitcheckout
 * @version      1.4.17 - 1.4.17
 * @license:     FoOiFpEBsy1CQtPSBPTqBFwAOZTnyw3qCMIlTMg96m
 * @copyright:   Copyright (c) 2017 AITOC, Inc. (http://www.aitoc.com)
 */
class Aitoc_Aitcheckout_Block_Giftreg_Indicator extends Mage_Core_Block_Template
{
    protected function _toHtml()
    {
        if(Mage::helper('aitcheckout/adjgiftregistry')->isEnabled())
        {
            return $this->getLayout()
                ->createBlock('adjgiftreg/indicator')
                ->setTemplate('adjgiftreg/indicator.phtml')
                ->toHtml()
            ;
        }
        return '';
    }
}