<?php
/**
 * All-In-One Checkout : All-In-One Checkout v1.1.0 (OPCB Unit)
 *
 * @package:     Aitoc_Aitcheckout / Aitoc_Aitcheckout
 * @version      1.4.17 - 1.4.17
 * @license:     FoOiFpEBsy1CQtPSBPTqBFwAOZTnyw3qCMIlTMg96m
 * @copyright:   Copyright (c) 2017 AITOC, Inc. (http://www.aitoc.com)
 */
class Aitoc_Aitcheckout_Block_Checkout_Giftwrap extends Mage_Core_Block_Template
{
    public function isShow()
    {
        return (Mage::helper('aitcheckout/aitgiftwrap')->isEnabled());
    }

    protected function _toHtml()
    {
        if($this->isShow())
        {
            $html = $this->getLayout()
                ->createBlock('aitgiftwrap/giftwrap_onepage')
                ->setTemplate('aitgiftwrap/giftwrap.phtml')
                ->toHtml();
               
            if($html)
            {
                return $html . parent::_toHtml();
            }
        }
        return '';
    }
}