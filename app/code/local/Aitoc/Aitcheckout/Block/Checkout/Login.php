<?php
/**
 * All-In-One Checkout : All-In-One Checkout v1.1.0 (OPCB Unit)
 *
 * @package:     Aitoc_Aitcheckout / Aitoc_Aitcheckout
 * @version      1.4.17 - 1.4.17
 * @license:     FoOiFpEBsy1CQtPSBPTqBFwAOZTnyw3qCMIlTMg96m
 * @copyright:   Copyright (c) 2017 AITOC, Inc. (http://www.aitoc.com)
 */
class Aitoc_Aitcheckout_Block_Checkout_Login extends Mage_Checkout_Block_Onepage_Abstract
{
    public function getCaptchaReloadUrl() {
        if(!$this->helper('aitcheckout/captcha')->checkIfCaptchaEnabled()) {
            return '';
        }
        $blockPath = Mage::helper('captcha')->getCaptcha('user_login')->getBlockName();
        $block = $this->getLayout()->createBlock($blockPath);
        return $block->getRefreshUrl();
    }

}