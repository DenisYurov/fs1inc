<?php
/**
 * All-In-One Checkout : All-In-One Checkout v1.1.0 (OPCB Unit)
 *
 * @package:     Aitoc_Aitcheckout / Aitoc_Aitcheckout
 * @version      1.4.17 - 1.4.17
 * @license:     FoOiFpEBsy1CQtPSBPTqBFwAOZTnyw3qCMIlTMg96m
 * @copyright:   Copyright (c) 2017 AITOC, Inc. (http://www.aitoc.com)
 */
if (version_compare(Mage::getVersion(), '1.7.0.0', '<'))
{
    class Aitoc_Aitcheckout_Block_Captcha extends Mage_Core_Block_Template
    {
    
    }
}
else
{
    class Aitoc_Aitcheckout_Block_Captcha extends Mage_Captcha_Block_Captcha
    {
    
        protected function _prepareLayout()
        {
            $headBlock = $this->getLayout()->getBlock('head');
			if($headBlock)
			{
				$headBlock->addJs('mage/captcha.js');
			}
			
            return parent::_prepareLayout();
        }    
    
    }
}