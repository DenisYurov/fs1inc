<?php
/**
 * All-In-One Checkout : All-In-One Checkout v1.1.0 (OPCB Unit)
 *
 * @package:     Aitoc_Aitcheckout / Aitoc_Aitcheckout
 * @version      1.4.17 - 1.4.17
 * @license:     FoOiFpEBsy1CQtPSBPTqBFwAOZTnyw3qCMIlTMg96m
 * @copyright:   Copyright (c) 2017 AITOC, Inc. (http://www.aitoc.com)
 */
class Aitoc_Aitcheckout_Helper_Onlyif_Data extends Aitoc_Aitcheckout_Helper_Abstract
{
    public function saveBilling($currentStep, $customerAddressId)
    {
        if($currentStep == 'payment' && Mage::helper('aitcheckout/aitconfcheckout')->isEnabled() && Mage::helper('customer')->isLoggedIn())
        {
            if (!Mage::getSingleton('checkout/type_onepage')->getQuote()->getBillingAddress()->getData('customer_address_id'))
            {
                if ($addId = Mage::app()->getRequest()->getPost('billing_address_id', false))
                {
                    $customerAddressId = $addId;
                }
                Mage::getSingleton('checkout/type_onepage')->saveBilling(Mage::app()->getRequest()->getPost('billing', array()), $customerAddressId);
            }
        }
    }
}