<?php
/**
 * All-In-One Checkout : All-In-One Checkout v1.1.0 (OPCB Unit)
 *
 * @package:     Aitoc_Aitcheckout / Aitoc_Aitcheckout
 * @version      1.4.17 - 1.4.17
 * @license:     FoOiFpEBsy1CQtPSBPTqBFwAOZTnyw3qCMIlTMg96m
 * @copyright:   Copyright (c) 2017 AITOC, Inc. (http://www.aitoc.com)
 */
class Aitoc_Aitcheckout_Helper_Login extends Aitoc_Aitcheckout_Helper_Abstract
{

    /**
     * @return boolean
     */
    private function _isCheckoutLoginPersistent()
	{
        return Mage::getConfig()->getModuleConfig('Mage_Persistent')->is('active', 'true');
    }
	
	/**
     * Return login block tempates. There are no persistent template in old versions of magento.
     *
     * @return string
     */
    public function getLoginTemplatePath()
	{
        if($this->_isCheckoutLoginPersistent()){
            return "persistent/checkout/onepage/login.phtml";
        }
        return  "checkout/onepage/login.phtml";
    }

}