<?php
/**
 * All-In-One Checkout : All-In-One Checkout v1.1.0 (CC Unit)
 *
 * @package:     Aitoc_Aitconfcheckout / Aitoc_Aitconfcheckout
 * @version      2.1.31 - 2.1.31
 * @license:     FoOiFpEBsy1CQtPSBPTqBFwAOZTnyw3qCMIlTMg96m
 * @copyright:   Copyright (c) 2017 AITOC, Inc. (http://www.aitoc.com)
 */
class Aitoc_Aitconfcheckout_Model_Rewrite_PaypalApiNvp extends Mage_Paypal_Model_Api_Nvp
{
    public function callDoExpressCheckoutPayment()
    {
        if(
            !Mage::getStoreConfig('aitconfcheckout/shipping/active') ||
            !Mage::getStoreConfig('aitconfcheckout/shipping/address') ||
            !Mage::getStoreConfig('aitconfcheckout/shipping/city') ||
            !Mage::getStoreConfig('aitconfcheckout/shipping/region') ||
            !Mage::getStoreConfig('aitconfcheckout/shipping/country') ||
            !Mage::getStoreConfig('aitconfcheckout/shipping/postcode') ||
            !Mage::getStoreConfig('aitconfcheckout/shipping/telephone')
          )
        {
            $this->setSuppressShipping(true);
        }

        parent::callDoExpressCheckoutPayment();
    }
}