<?php
/**
 * All-In-One Checkout : All-In-One Checkout v1.1.0 (CC Unit)
 *
 * @package:     Aitoc_Aitconfcheckout / Aitoc_Aitconfcheckout
 * @version      2.1.31 - 2.1.31
 * @license:     FoOiFpEBsy1CQtPSBPTqBFwAOZTnyw3qCMIlTMg96m
 * @copyright:   Copyright (c) 2017 AITOC, Inc. (http://www.aitoc.com)
 */
class Aitoc_Aitconfcheckout_Helper_Paypal extends Mage_Core_Helper_Abstract
{
    protected $configs = array();

    public function __construct()
    {
        foreach(array('billing','shipping') as $sType)
        {
            if(!isset($this->_configs[$sType]))
            {
                $this->_configs[$sType] = array();
            }

            $aAllowedFieldHash = Mage::helper('aitconfcheckout')->getAllowedFieldHash($sType);

            foreach ($aAllowedFieldHash as $sKey => $bValue)
            {
                $this->_configs[$sType][$sKey] = $bValue;
            }
        }

    }

    public function checkFieldShow($sType,$sKey)
    {
        if (!$sKey || !isset($this->_configs[$sType]) || !isset($this->_configs[$sType][$sKey]))
        {
            return false;
        }

        if ($this->_configs[$sType][$sKey])
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}