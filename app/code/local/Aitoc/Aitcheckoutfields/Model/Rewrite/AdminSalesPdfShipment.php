<?php
/**
 * All-In-One Checkout : All-In-One Checkout v1.1.0 (CFM Unit)
 *
 * @package:     Aitoc_Aitcheckoutfields / Aitoc_Aitcheckoutfields
 * @version      2.10.2 - 2.10.2
 * @license:     FoOiFpEBsy1CQtPSBPTqBFwAOZTnyw3qCMIlTMg96m
 * @copyright:   Copyright (c) 2017 AITOC, Inc. (http://www.aitoc.com)
 */
class Aitoc_Aitcheckoutfields_Model_Rewrite_AdminSalesPdfShipment extends Mage_Sales_Model_Order_Pdf_Shipment
{
    protected function _drawCheckoutAttributes($page, $obj)
    {
        $data = Mage::getModel('aitcheckoutfields/aitcheckoutfields')->getCustomData($obj->getOrder()->getId(), 'shipment', null, true);
        if (!empty($data)) {   
            $charsPerLine   = 140;
            $heightPerLine  = 10;
            $count_of_lines = 0;

            foreach ($data as $attr) {
                $text  = htmlspecialchars_decode($attr['value']);
                $text  = wordwrap($text, $charsPerLine, "<br />");
                $lines = explode("<br />", $text);
                $count_of_lines += count($lines);
            }

            $page->setFillColor(new Zend_Pdf_Color_Rgb(0.93, 0.92, 0.92));
            $this->y -= 10;
            $page->drawRectangle(25, $this->y, 570, $this->y - 25);
            $headTitle = Mage::getStoreConfig('aitcheckoutfields/common_settings/aitcheckoutfields_additionalblock_label');
            $page->setFillColor(new Zend_Pdf_Color_Rgb(0, 0, 0));
            $this->_setFontBold($page);
            $page->drawText($headTitle, 35, $this->y - 15 , 'UTF-8');  
            $this->y -= 25;
            $page->setFillColor(new Zend_Pdf_Color_Rgb(1, 1, 1));  
            $page->drawRectangle(25, $this->y, 570, $this->y - 10 * $count_of_lines - 5);
            $this->_setFontRegular($page);   
            $page->setFillColor(new Zend_Pdf_Color_Rgb(0, 0, 0));        
            $this->y -= 5;

            foreach ($data as $attr) {
                $page->setFillColor(new Zend_Pdf_Color_Rgb(0, 0, 0));
                $startPos = 30;
                $text  = htmlspecialchars_decode($attr['value']);
                $text  = wordwrap($text, $charsPerLine, "<br />");
                $lines = explode("<br />", $text);

                    foreach ($lines as $i => $line) {
                        if ($i == 0) {
                            $page->drawText($attr['label'] . ': ', $startPos, $this->y - 4, 'UTF-8');
                            $page->drawText($line, $startPos + 100, $this->y - 4, 'UTF-8');
                        } else {
                            $page->drawText($line, $startPos + 100, $this->y - 4, 'UTF-8');
                        }
                        $this->y -= $heightPerLine;
                    }
            }

            $this->y -= 15;
        }
    }

    protected function insertOrder(&$page, $obj, $putOrderId = true)
    {
        parent::insertOrder($page, $obj, $putOrderId);
        $this->_drawCheckoutAttributes($page, $obj);
    }
}