<?php
/**
 * All-In-One Checkout : All-In-One Checkout v1.1.0 (CFM Unit)
 *
 * @package:     Aitoc_Aitcheckoutfields / Aitoc_Aitcheckoutfields
 * @version      2.10.2 - 2.10.2
 * @license:     FoOiFpEBsy1CQtPSBPTqBFwAOZTnyw3qCMIlTMg96m
 * @copyright:   Copyright (c) 2017 AITOC, Inc. (http://www.aitoc.com)
 */
/**
 * @copyright  Copyright (c) 2009 AITOC, Inc. 
 */
/* AITOC static rewrite inserts start */
/* $meta=%default,AdjustWare_Deliverydate,AdjustWare_Notification% */
if(Mage::helper('core')->isModuleEnabled('AdjustWare_Notification')){
    class Aitoc_Aitcheckoutfields_Model_Rewrite_FrontSalesOrder_Aittmp extends AdjustWare_Notification_Model_Rewrite_Sales_Order {} 
 }elseif(Mage::helper('core')->isModuleEnabled('AdjustWare_Deliverydate')){
    class Aitoc_Aitcheckoutfields_Model_Rewrite_FrontSalesOrder_Aittmp extends AdjustWare_Deliverydate_Model_Rewrite_FrontSalesOrder {} 
 }else{
    /* default extends start */
    class Aitoc_Aitcheckoutfields_Model_Rewrite_FrontSalesOrder_Aittmp extends Mage_Sales_Model_Order {}
    /* default extends end */
}

/* AITOC static rewrite inserts end */
class Aitoc_Aitcheckoutfields_Model_Rewrite_FrontSalesOrder extends Aitoc_Aitcheckoutfields_Model_Rewrite_FrontSalesOrder_Aittmp
{
    protected $_cfmCustomFields;

    /**
     * Get CFM data for this order
     * 
     * @param bool $forceReload Forces tranport object to be reloaded. Default: false.
     * 
     * @return Aitoc_Aitcheckoutfields_Model_Transport
     */    
    public function getCustomFields($forceReload = false)
    {
        if(is_null($this->_cfmCustomFields) || $forceReload)
        {
            $this->_cfmCustomFields = Mage::getModel('aitcheckoutfields/transport')->loadByOrder($this);
        }
        return $this->_cfmCustomFields;
    }
}