<?php
/**
 * All-In-One Checkout : All-In-One Checkout v1.1.0 (CFM Unit)
 *
 * @package:     Aitoc_Aitcheckoutfields / Aitoc_Aitcheckoutfields
 * @version      2.10.2 - 2.10.2
 * @license:     FoOiFpEBsy1CQtPSBPTqBFwAOZTnyw3qCMIlTMg96m
 * @copyright:   Copyright (c) 2017 AITOC, Inc. (http://www.aitoc.com)
 */
class Aitoc_Aitcheckoutfields_Block_Rewrite_FrontCustomerFormEdit extends Mage_Customer_Block_Account_Dashboard
{
    protected $_mainModel;
    
    protected function _construct()
    {
        parent::_construct();
        $this->_mainModel = Mage::getModel('aitcheckoutfields/aitcheckoutfields');
    }
    
    public function getCustomFieldsList($placeholder)
    {
        return $this->_mainModel->getCustomerAttributeList($placeholder);
    }
    
    public function getAttributeHtml($aField, $sSetName, $sPageType)
    {
        return $this->_mainModel->getAttributeHtml($aField, $sSetName, $sPageType);
    }
}