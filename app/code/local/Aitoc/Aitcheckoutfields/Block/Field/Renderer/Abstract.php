<?php
/**
 * All-In-One Checkout : All-In-One Checkout v1.1.0 (CFM Unit)
 *
 * @package:     Aitoc_Aitcheckoutfields / Aitoc_Aitcheckoutfields
 * @version      2.10.2 - 2.10.2
 * @license:     FoOiFpEBsy1CQtPSBPTqBFwAOZTnyw3qCMIlTMg96m
 * @copyright:   Copyright (c) 2017 AITOC, Inc. (http://www.aitoc.com)
 */
class Aitoc_Aitcheckoutfields_Block_Field_Renderer_Abstract  extends Mage_Core_Block_Abstract
{
    public function setParams(array $data) {
        foreach($data as $key => $value)
        {
            $this->$key=$value;
        }
        return $this;
    }
    
    public function render()
    {
        return '';
    }
}

?>