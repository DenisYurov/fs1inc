<?php
/**
 * All-In-One Checkout : All-In-One Checkout v1.1.0 (CFM Unit)
 *
 * @package:     Aitoc_Aitcheckoutfields / Aitoc_Aitcheckoutfields
 * @version      2.10.2 - 2.10.2
 * @license:     FoOiFpEBsy1CQtPSBPTqBFwAOZTnyw3qCMIlTMg96m
 * @copyright:   Copyright (c) 2017 AITOC, Inc. (http://www.aitoc.com)
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Date
 *
 * @author kirichenko
 */
class Aitoc_Aitcheckoutfields_Block_Field_Renderer_Multiselect extends Aitoc_Aitcheckoutfields_Block_Field_Renderer_Abstract 
{
    public function render() 
    {
        $values = explode(',', $this->sFieldValue[0]);

        $select = Mage::getModel('core/layout')->createBlock('core/html_select')
                    ->setName($this->sFieldName . '[]')
                    ->setId($this->sFieldId)
                    ->setTitle($this->sLabel)
                    ->setClass($this->sFieldClass)
                    ->setValue($values)
                    ->setExtraParams('multiple')
                    ->setOptions($this->aOptionHash);
                
                    $sHidden = '<input type="hidden" name="'.$this->sFieldName.'"  value="" />';
                    
                    return $sHidden . $select->getHtml();
    }
}

?>