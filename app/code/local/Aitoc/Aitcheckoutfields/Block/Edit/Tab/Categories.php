<?php
/**
 * All-In-One Checkout : All-In-One Checkout v1.1.0 (CFM Unit)
 *
 * @package:     Aitoc_Aitcheckoutfields / Aitoc_Aitcheckoutfields
 * @version      2.10.2 - 2.10.2
 * @license:     FoOiFpEBsy1CQtPSBPTqBFwAOZTnyw3qCMIlTMg96m
 * @copyright:   Copyright (c) 2017 AITOC, Inc. (http://www.aitoc.com)
 */
class Aitoc_Aitcheckoutfields_Block_Edit_Tab_Categories extends Mage_Adminhtml_Block_Catalog_Product_Edit_Tab_Categories
{
    public function getCategoryIds() {
        return Mage::getModel('aitcheckoutfields/attributecatalogrefs')->getRefs(Mage::app()->getRequest()->getParam('attribute_id'),'category');
    }
}

?>