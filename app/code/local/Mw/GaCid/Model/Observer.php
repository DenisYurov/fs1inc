<?php
class Mw_GaCid_Model_Observer
{
	public function SetClientId(Varien_Event_Observer $observer)
	{
		$userAgent = $_SERVER['HTTP_USER_AGENT'];
		$order = $observer->getEvent()->getOrder();
		$gacid = Mage::app()->getRequest()->getPost('gacid');
		Mage::log($gacid,null,"test.log");
		$agent = urlencode($userAgent);
		Mage::log($agent,null,"test.log");
		$order->setClientId($gacid);
		$order->setUserAgent($agent);
		//$order->save();
	}
		
}
