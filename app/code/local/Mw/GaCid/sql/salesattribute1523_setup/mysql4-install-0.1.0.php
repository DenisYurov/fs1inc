<?php
$installer = $this;
$installer->startSetup();

$installer->addAttribute("order", "client_id", array("type"=>"varchar"));
$installer->addAttribute("order", "user_agent", array("type"=>"varchar"));
$installer->addAttribute("order", "ga_sent", array("type"=>"boolean"));
$installer->endSetup();
	 