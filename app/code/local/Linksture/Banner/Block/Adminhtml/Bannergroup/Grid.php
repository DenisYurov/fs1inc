<?php
class Linksture_Banner_Block_Adminhtml_Bannergroup_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct() {
        parent::__construct();
        $this->setId('bannergroupGrid');
        $this->setDefaultSort('group_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection() {
        $collection = Mage::getModel('banner/bannergroup')->getCollection();
        //$collection->getSelect()->columns(array('banner_effect' => 'if((animation_type=0),pre_banner_effects,banner_effects)'));
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {
        $this->addColumn('group_id', array(
            'header' => Mage::helper('banner')->__('ID'),
            'align' => 'center',
            'width' => '50px',
            'index' => 'group_id',
        ));

        $this->addColumn('group_name', array(
            'header' => Mage::helper('banner')->__('Name'),
            'index' => 'group_name',
        ));
        
        $this->addColumn('group_code', array(
            'header' => Mage::helper('banner')->__('Block Code ID'),
            'index' => 'group_code',
        ));
        
        $this->addColumn('transition_style', array(
            'header' => Mage::helper('banner')->__('Transition Style'),
            'align' => 'left',
            'index' => 'transition_style',
            'type' => 'options',
            'options' => array(
                'backSlide' => 'Back Slide',
                'fade' => 'Fade',
                'fadeUp' => 'Fade Up',
                'goDown' => 'Go Down',
                'slide' => 'Slide',
            ),
        ));

        $this->addColumn('status', array(
            'header' => Mage::helper('banner')->__('Status'),
            'align' => 'left',
            'index' => 'status',
            'type' => 'options',
            'options' => array(
                1 => 'Enabled',
                2 => 'Disabled',
            ),
        ));

        $this->addColumn('copy_shortcode', array(
            'header' => Mage::helper('banner')->__('Shortcode for CMS / HTML Editor'),
            'index'  => 'copy_shortcode',
            'width'  => '900px',
            'sortable' => false,
            'filter' => false,
            'renderer' => 'Linksture_Banner_Block_Adminhtml_Banner_Renderer_Image',
        ));

        $this->addColumn('action',
                array(
                    'header' => Mage::helper('banner')->__('Action'),
                    'width' => '50',
                    'type' => 'action',
                    'getter' => 'getId',
                    'actions' => array(
                        array(
                            'caption' => Mage::helper('banner')->__('Edit'),
                            'url' => array('base' => '*/*/edit'),
                            'field' => 'id'
                        )
                    ),
                    'filter' => false,
                    'sortable' => false,
                    'index' => 'stores',
                    'is_system' => true,
        ));

        $this->addExportType('*/*/exportCsv', Mage::helper('banner')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('banner')->__('XML'));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction() {
        $this->setMassactionIdField('group_id');
        $this->getMassactionBlock()->setFormFieldName('banner');

        $this->getMassactionBlock()->addItem('delete', array(
            'label' => Mage::helper('banner')->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
            'confirm' => Mage::helper('banner')->__('Are you sure?')
        ));

        $statuses = Mage::getSingleton('banner/status')->getOptionArray();

        array_unshift($statuses, array('label' => '', 'value' => ''));
        $this->getMassactionBlock()->addItem('status', array(
            'label' => Mage::helper('banner')->__('Change status'),
            'url' => $this->getUrl('*/*/massStatus', array('_current' => true)),
            'additional' => array(
                'visibility' => array(
                    'name' => 'status',
                    'type' => 'select',
                    'class' => 'required-entry',
                    'label' => Mage::helper('banner')->__('Status'),
                    'values' => $statuses
                )
            )
        ));
        return $this;
    }

    public function getRowUrl($row) {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
}