<?php
class Linksture_Banner_Block_Adminhtml_Bannergroup_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct() {
        parent::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'banner';
        $this->_controller = 'adminhtml_bannergroup';

        $this->_updateButton('save', 'label', Mage::helper('banner')->__('Save Banner Slider'));
        $this->_updateButton('delete', 'label', Mage::helper('banner')->__('Delete'));

        $this->_addButton('saveandcontinue', array(
            'label' => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick' => 'saveAndContinueEdit()',
            'class' => 'save',
                ), -100);

        $this->_formScripts[] = "function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }";
    }

    public function getHeaderText() {
        if (Mage::registry('bannergroup_data') && Mage::registry('bannergroup_data')->getId()) {
            return Mage::helper('banner')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('bannergroup_data')->getGroupName()));
        } else {
            return Mage::helper('banner')->__('Add Banner Slider');
        }
    }
}