<?php
class Linksture_Banner_Block_Adminhtml_Bannergroup_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm() {
        $form = new Varien_Data_Form();
        
        $this->setForm($form);

        $fieldset = $form->addFieldset('bannergroup_form', array('legend' => Mage::helper('banner')->__('Banner Slider Information')));
        $fieldsetConfig = $form->addFieldset('bannergroup_configuration', array('legend' => Mage::helper('banner')->__('Banner Slider Configuration')));

        $fieldset->addField('group_name', 'text', array(
            'label' => Mage::helper('banner')->__('Name'),
            'required' => true,
            'name' => 'group_name',
        ));

        if (Mage::registry('bannergroup_data')->getId() == null) {
            $disabled = "";
        } else {
            $disabled = "'disabled'";
        }
        
        $fieldset->addField('group_code', 'text', array(
            'label' => Mage::helper('banner')->__('Block Code ID'),
            'name' => 'group_code',
            'required' => true,
            'disabled' => $disabled
        ));

        $fieldset->addField('show_title', 'select', array(
            'label' => Mage::helper('banner')->__('Show Image Title in Banner'),
            'name' => 'show_title',
            'values' => array(
                array(
                    'value' => 1,
                    'label' => Mage::helper('banner')->__('Yes'),
                ),
                array(
                    'value' => 0,
                    'label' => Mage::helper('banner')->__('No'),
                ),
            ),
        ));

        $fieldset->addField('show_content', 'select', array(
            'label' => Mage::helper('banner')->__('Show Description in Banner'),
            'name' => 'show_content',
            'values' => array(
                array(
                    'value' => 1,
                    'label' => Mage::helper('banner')->__('Yes'),
                ),
                array(
                    'value' => 0,
                    'label' => Mage::helper('banner')->__('No'),
                ),
            )
        ));

        $fieldset->addField('status', 'select', array(
            'label' => Mage::helper('banner')->__('Status'),
            'name' => 'status',
            'values' => array(
                array(
                    'value' => 1,
                    'label' => Mage::helper('banner')->__('Enabled'),
                ),
                array(
                    'value' => 2,
                    'label' => Mage::helper('banner')->__('Disabled'),
                ),
            ),
        ));

        $fieldset->addField('slide_show_style', 'radios', array(
            'label' => Mage::helper('banner')->__('Slide Show Style'),
            'name' => 'slide_show_style',
            'values' => array(
                array('value' => 1,'label'=>'<img src="'.$this->getSkinUrl('images/bannerslider/style-1.jpg').'" width="265" style="vertical-align: middle; margin:5px 0px 5px 5px;" />'),
                array('value' => 0,'label'=>'<img src="'.$this->getSkinUrl('images/bannerslider/style-2.jpg').'" width="265" style="vertical-align: middle; margin:5px 0px 5px 5px;" />'),
                )
        ));

        $fieldsetConfig->addField('autoplay', 'select', array(
            'label' => Mage::helper('banner')->__('Auto Play Slideshow'),
            'name' => 'autoplay',
            'values' => array(
                array(
                    'value' => 1,
                    'label' => Mage::helper('banner')->__('Yes'),
                ),
                array(
                    'value' => 0,
                    'label' => Mage::helper('banner')->__('No'),
                ),
            ),
        ));

        $fieldsetConfig->addField('slide_interval', 'text', array(
            'label' => Mage::helper('banner')->__('Slide Interval'),
            'name' => 'slide_interval',
            'after_element_html' => '<br />In milliseconds [eg: 1 Second = 1000 Millisecond]'
        ));

        $fieldsetConfig->addField('animation_speed', 'text', array(
            'label' => Mage::helper('banner')->__('Slide Animation Speed'),
            'name' => 'animation_speed',
            'after_element_html' => '<br />In milliseconds [eg: 1 Second = 1000 Millisecond]'
        ));

        $fieldsetConfig->addField('transition_style', 'select', array(
            'label' => Mage::helper('banner')->__('Transition Style'),
            'name' => 'transition_style',
            'values' => array(
                array(
                    'value' => '',
                    'label' => Mage::helper('banner')->__('-- Please Select --'),
                ),
                array(
                    'value' => 'backSlide',
                    'label' => Mage::helper('banner')->__('Back Slide'),
                ),
                array(
                    'value' => 'fade',
                    'label' => Mage::helper('banner')->__('Fade'),
                ),
                array(
                    'value' => 'fadeUp',
                    'label' => Mage::helper('banner')->__('Fade Up'),
                ),
                array(
                    'value' => 'goDown',
                    'label' => Mage::helper('banner')->__('Go Down'),
                ),
                array(
                    'value' => 'slide',
                    'label' => Mage::helper('banner')->__('Slide'),
                ),
            ),
            'required' => true,
        ));

        $fieldsetConfig->addField('show_next_prev', 'select', array(
            'label' => Mage::helper('banner')->__('Show Next / Previous Control'),
            'name' => 'show_next_prev',
            'values' => array(
                array(
                    'value' => 1,
                    'label' => Mage::helper('banner')->__('Yes'),
                ),
                array(
                    'value' => 0,
                    'label' => Mage::helper('banner')->__('No'),
                ),
            ),
        ));

        $fieldsetConfig->addField('show_pager', 'select', array(
            'label' => Mage::helper('banner')->__('Show Pager Navigation Control'),
            'name' => 'show_pager',
            'values' => array(
                array(
                    'value' => 1,
                    'label' => Mage::helper('banner')->__('Yes'),
                ),
                array(
                    'value' => 0,
                    'label' => Mage::helper('banner')->__('No'),
                ),
            ),
        ));

        if (Mage::getSingleton('adminhtml/session')->getBannergroupData()) {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getBannergroupData());
            Mage::getSingleton('adminhtml/session')->setBannergroupData(null);
        } elseif (Mage::registry('bannergroup_data')) {
            $form->setValues(Mage::registry('bannergroup_data')->getData());
        }

        return parent::_prepareForm();
    }
}