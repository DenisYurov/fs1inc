<?php
class Linksture_Banner_Block_Adminhtml_Bannergroup_Edit_Tab_Gridbanner extends Mage_Adminhtml_Block_Widget_Container
{
    /**
     * Set template
     */
    public function __construct() {
        parent::__construct();
        $this->setTemplate('banner/banners.phtml');
    }

    public function getTabsHtml() {
        return $this->getChildHtml('tabs');
    }

    /**
     * Prepare button and grid
     *
     */
    protected function _prepareLayout() {
        $this->setChild('tabs', $this->getLayout()->createBlock('banner/adminhtml_bannergroup_edit_tab_banner', 'bannergroup.grid.banner'));
        return parent::_prepareLayout();
    }

    public function getBannergroupData() {
        return Mage::registry('bannergroup_data');
    }

    public function getBannersJson() {
        $banners = explode(',', $this->getBannergroupData()->getBannerIds());
        if (!empty($banners) && isset($banners[0]) && $banners[0] != '') {
            $data = array();
            foreach ($banners as $element) {
                $data[$element] = $element;
            }
            return Zend_Json::encode($data);
        }
        return '{}';
    }
    public function getSortOrderJson() {
        $collection = Mage::getModel('banner/banner')->getCollection();
        $gropu_id = $this->getRequest()->getParam('id');
        if($gropu_id) {
            $resource = Mage::getSingleton('core/resource');
            $collection->getSelect()
                ->joinLeft(array('short_order' => $resource->getTableName('linksture_shortorder')),'short_order.banner_id = main_table.banner_id AND short_order.group_id='.$gropu_id)
                ->reset(Zend_Db_Select::COLUMNS)
                ->columns(array('main_table.banner_id','short_order.value'));
        }        
        
        $sortOrder = array();
        if(isset($collection) && count($collection) > 1) {
            foreach ($collection->getData() as $value) {
                if(isset($value["value"]) && $value["value"] >= 0) {
                    $sortOrder[$value["banner_id"]] = $value["value"];
                }
            }

            if(count($sortOrder) <= 0) {
                return '{}';    
            } else {
                return Zend_Json::encode($sortOrder);
            }            
        } else {
            return '{}';
        }        
    }
}
