<?php
class Linksture_Banner_Block_Adminhtml_Bannergroup_Edit_Tab_Banner extends Linksture_Banner_Block_Adminhtml_Widget_Grid
{
    public function __construct() {
        parent::__construct();
        $this->setId('bannerLeftGrid');
        $this->setDefaultSort('banner_id');
        $this->setDefaultDir('ASC');
        $this->setUseAjax(true);
    }

    public function getBannergroupData() {
        return Mage::registry('bannergroup_data');
    }

    protected function _prepareCollection() {
        $collection = Mage::getModel('banner/banner')->getCollection();
        $gropu_id = $this->getRequest()->getParam('id');
        if($gropu_id) {
            $resource = Mage::getSingleton('core/resource');
            $collection->getSelect()
                ->joinLeft(array('short_order' => $resource->getTableName('linksture_shortorder')),'short_order.banner_id = main_table.banner_id and short_order.group_id='.$gropu_id)
                ->reset(Zend_Db_Select::COLUMNS)
                ->columns(array('main_table.banner_id','main_table.filename','main_table.title','main_table.link','main_table.banner_content','short_order.value'));
        }
       
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _addColumnFilterToCollection($column) {
        if ($this->getCollection()) {
            if ($column->getId() == 'banner_triggers') {
                $bannerIds = $this->_getSelectedBanners();
                if (empty($bannerIds)) {
                    $bannerIds = 0;
                }
                if ($column->getFilter()->getValue()) {
                    $this->getCollection()->addFieldToFilter('main_table.banner_id', array('in' => $bannerIds));
                } else {
                    if ($bannerIds) {
                        $this->getCollection()->addFieldToFilter('main_table.banner_id', array('nin' => $bannerIds));
                    }
                }
            } else {
                parent::_addColumnFilterToCollection($column);
            }
        }
        return $this;
    }

    protected function _prepareColumns() {
        $this->addColumn('banner_triggers', array(
            'header_css_class' => 'a-center',
            'type' => 'checkbox',
            'values' => $this->_getSelectedBanners(),
            'align' => 'center',
            'index' => 'banner_id',
        ));
        $this->addColumn('banner_id', array(
            'header' => Mage::helper('catalog')->__('ID'),
            'sortable' => true,
            'width' => '40',
            'align' => 'center',
            'index' => 'banner_id',
            'filter_index' => 'main_table.banner_id',
        ));

        $this->addColumn('filename', array(
            'header' => Mage::helper('banner')->__('Image'),
            'align' => 'center',
            'index' => 'filename',
            'type' => 'banner',
            'width' => '100px',
            'sortable' => false,
            'filter' => false,
        ));

        $this->addColumn('title', array(
            'header' => Mage::helper('catalog')->__('Title'),
            'index' => 'title',
            'align' => 'left',
        ));

        $this->addColumn('link', array(
            'header' => Mage::helper('banner')->__('Link URL'),
            'index' => 'link',
        ));

        $this->addColumn('banner_content', array(
            'header' => Mage::helper('banner')->__('Description'),
            'index' => 'banner_content',
        ));

        $this->addColumn('sort_order', array(
            'header'=> Mage::helper('banner')->__('Sort Order'),
            'width' => '100',
            'index' => 'value',
            'align' => 'center',
            'type'  => 'number',
            'editable' => true,
            'filter' => false,
        ));
        return parent::_prepareColumns();
    }

    public function getGridUrl() {
        return $this->getUrl('*/*/bannergrid', array('_current' => true));
    }

    protected function _getSelectedBanners() {
        $banners = $this->getRequest()->getPost('selected_banners');
        if (is_null($banners)) {
            $banners = explode(',', $this->getBannergroupData()->getBannerIds());
            return (sizeof($banners) > 0 ? $banners : 0);
        }
        return $banners;
    }
}
?>