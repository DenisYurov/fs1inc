<?php
class Linksture_Banner_Block_Adminhtml_Banner_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm() {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('banner_form', array('legend' => Mage::helper('banner')->__('Banner Information')));
        $version = substr(Mage::getVersion(), 0, 3);

        $fieldset->addField('title', 'text', array(
            'label' => Mage::helper('banner')->__('Title'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'title',
        ));
        
        $fieldset->addField('filename', 'image', array(
            'label' => Mage::helper('banner')->__('Image'),
            'name' => 'filename',
        ));

        $fieldset->addField('link', 'text', array(
            'label' => Mage::helper('banner')->__('Link URL'),
            'name' => 'link',
           
        ));

        $fieldset->addField('action_text', 'text', array(
            'label' => Mage::helper('banner')->__('Call to Action Text'),
            'name' => 'action_text',
            'after_element_html' => 'Example: Buy Now, Shop Now, Read More, etc...'
        ));

        $fieldset->addField('status', 'select', array(
            'label' => Mage::helper('banner')->__('Status'),
            'class' => 'required-entry',
            'name' => 'status',
            'values' => array(
                array(
                    'value' => 1,
                    'label' => Mage::helper('banner')->__('Enabled'),
                ),
                array(
                    'value' => 2,
                    'label' => Mage::helper('banner')->__('Disabled'),
                ),
            ),
        ));

        $fieldset->addField('banner_content', 'editor', array(
                'name' => 'banner_content',
                'label' => Mage::helper('cms')->__('Description'),
                'title' => Mage::helper('cms')->__('Description')
        ));

        if (Mage::getSingleton('adminhtml/session')->getBannerData()) {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getBannerData());
            Mage::getSingleton('adminhtml/session')->setBannerData(null);
        } elseif (Mage::registry('banner_data')) {
            $form->setValues(Mage::registry('banner_data')->getData());
        }
        return parent::_prepareForm();
    }
}