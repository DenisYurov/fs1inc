<?php  
class Linksture_Banner_Block_Adminhtml_Banner_Renderer_Image extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row) {

        return '<span style="font-weight:bold;" id="'.strtolower($row->getData('group_code')).'">{{block type="banner/banner" name="'.str_replace(" ", ".", strtolower($row->getData('group_name'))).'" banner_group_code="'.$row->getData('group_code').'" template="banner/banner.phtml"}}</span><a href="javascript:void(0);" style="float:right;" onclick="selectCode('."'".strtolower($row->getData("group_code"))."'".');">Select & Copy</a>
        <script type="text/javascript">
            function selectCode(shortcodeId) {
                if (document.selection) {
                    var range = document.body.createTextRange();
                    range.moveToElementText(document.getElementById(shortcodeId));
                    range.select();
                } else if (window.getSelection) {
                    var range = document.createRange();
                    range.selectNode(document.getElementById(shortcodeId));
                    window.getSelection().addRange(range);
                }
            }
        </script>';
    }    
    // public function render(Varien_Object $row){
    //     $mediaurl=Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
    //     $value = $row->getData($this->getColumn()->getIndex());
    //     return '<p style="text-align:center;padding-top:10px;"><img src="'.$mediaurl.$value.'"  style="width:100px;height:200px;text-align:center;"/></p>';
    // } 
}