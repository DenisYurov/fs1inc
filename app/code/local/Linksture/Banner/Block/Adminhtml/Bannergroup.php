<?php
class Linksture_Banner_Block_Adminhtml_Bannergroup extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct() {
        $this->_controller = 'adminhtml_bannergroup';
        $this->_blockGroup = 'banner';
        $this->_headerText = Mage::helper('banner')->__('Manage Banner Sliders');
        $this->_addButtonLabel = Mage::helper('banner')->__('Add New Slider');
        parent::__construct();
    }
}