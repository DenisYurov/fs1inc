<?php
class Linksture_Banner_Model_Banner extends Mage_Core_Model_Abstract
{
    public function _construct() {
        parent::_construct();
        $this->_init('banner/banner');
    }

    public function getAllAvailBannerIds(){
        $collection = Mage::getResourceModel('banner/banner_collection')
                        ->getAllIds();
        return $collection;
    }

    public function getAllBanners() {
        $collection = Mage::getResourceModel('banner/banner_collection');
        $collection->getSelect()->where('status = ?', 1);
        $data = array();
        foreach ($collection as $record) {
            $data[$record->getId()] = array('value' => $record->getId(), 'label' => $record->getfilename());
        }
        return $data;
    }

    public function getDataByBannerIds($bannerIds,$gropu_id) {
        $data = array();
        if ($bannerIds != '') {
            $resource = Mage::getSingleton('core/resource');
            $collection = Mage::getResourceModel('banner/banner_collection');
            $collection->getSelect()
            ->joinLeft(array('short_order' => $resource->getTableName('linksture_shortorder')),'short_order.banner_id = main_table.banner_id and short_order.group_id='.$gropu_id)
            ->where('main_table.banner_id IN (' . $bannerIds . ')')
            ->order("short_order.value");
            foreach ($collection as $record) {
                $status = $record->getStatus();
                if ($status == 1) {
                    $data[] = $record;
                }
            }
        }
        return $data;
    }
}