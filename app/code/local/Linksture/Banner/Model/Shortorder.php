<?php
class Linksture_Banner_Model_Shortorder extends Mage_Core_Model_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init('banner/shortorder');
    }
    public function updateData($short_order_value) 
    {
    	$connection = Mage::getSingleton('core/resource')->getConnection('core_write');
    	$short_order_collection = $this->getCollection()
    	->addFieldToFilter('banner_id', $short_order_value['banner_id'])
    	->addFieldToFilter('group_id', $short_order_value['group_id']);
    	$count_record = $this->getCollection()
    	->addFieldToFilter('banner_id', $short_order_value['banner_id'])
    	->addFieldToFilter('group_id', $short_order_value['group_id'])
    	->getData();
    	if(count($count_record)) {	
            if($short_order_value['value'] == "") {
                $short_order_value['value'] = "0";
            } else {
                $short_order_value['value'] = $short_order_value['value'];
            }
            $resource = Mage::getSingleton('core/resource');
            $update = "UPDATE ".$resource->getTableName('linksture_shortorder')." SET group_id = ".$short_order_value['group_id'].",
                `banner_id` = ".$short_order_value['banner_id'].", `value` = ".$short_order_value['value']." 
                WHERE `group_id` = ".$short_order_value['group_id']." and `banner_id` = ".$short_order_value['banner_id'];

	        $connection->query($update);
	        $connection->commit();

	    } else {
	    	$this->setData($short_order_value)->save();
	    }
    }
    public function deleteExtral($banner_id,$group_id)
    {
        $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
        $resource = Mage::getSingleton('core/resource');
        $update = "DELETE FROM ".$resource->getTableName('linksture_shortorder')." WHERE `group_id` = ".$group_id." AND `banner_id` NOT IN ($banner_id)";

            $connection->query($update);
            $connection->commit();

        
    }
}