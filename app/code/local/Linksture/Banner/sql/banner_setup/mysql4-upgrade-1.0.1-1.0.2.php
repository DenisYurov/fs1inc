<?php
if (version_compare(Mage::getVersion(), '1.9.2.2', '>=')) {
    $installer = $this;
    $connection = $installer->getConnection();

    $installer->startSetup();
    $installer->getConnection()->insertMultiple(
        $installer->getTable('admin/permission_block'),
        array(
            array('block_name' => 'banner/banner', 'is_allowed' => 1)
        )
    );
    $installer->endSetup();
}