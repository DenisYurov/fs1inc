<?php
$installer = $this;

$installer->startSetup();

$installer->run("

CREATE TABLE IF NOT EXISTS {$this->getTable('linksture_banner')} (
  `banner_id` int(11) unsigned NOT NULL auto_increment,
  `title` varchar(255) NOT NULL default '',
  `filename` varchar(255) NOT NULL default '',
  `link` varchar(255) NOT NULL default '',
  `banner_content` text NOT NULL default '',
  `status` smallint(6) NOT NULL default '0',
  `sort_order` int(11) NOT NULL default '0',
  `action_text` varchar(25) NULL,
  `created_time` DATETIME NULL,
  `update_time` DATETIME NULL,
  PRIMARY KEY (`banner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS {$this->getTable('linksture_bannergroup')} (
 `group_id` int(11) unsigned NOT NULL auto_increment,
 `group_name` varchar(255) NOT NULL default '',
 `group_code` varchar(255) NOT NULL default '',
 `banner_ids` varchar(255) NOT NULL default '',
 `show_title` TINYINT(4) NOT NULL default '0',
 `show_content` TINYINT(4) NOT NULL default '0',
 `status` smallint(6) NOT NULL default '0',
 `slide_show_style` smallint(6) NOT NULL default '0',
 `autoplay` smallint(6) NOT NULL default '0',
 `animation_speed` smallint(6) NOT NULL default '1000',
 `show_next_prev` smallint(6) NOT NULL default '0',
 `show_pager` smallint(6) NOT NULL default '0',
 `created_time` DATETIME NULL,
 `update_time` DATETIME NULL,
 PRIMARY KEY (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS {$this->getTable('linksture_shortorder')} (
 `id` int(11) unsigned NOT NULL auto_increment,
 `group_id` int(11) NOT NULL default '0',
 `banner_id` int(11) NOT NULL default '0',
 `value` int(11) NOT NULL default '0',
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

");

$installer->endSetup();
