<?php

$installer = $this;

$installer->startSetup();

$installer->run("
ALTER TABLE `" . $this->getTable('linksture_bannergroup') . "`
ADD `slide_interval` smallint(6) NOT NULL default '5000' AFTER `autoplay`,
ADD `transition_style` VARCHAR(255) NOT NULL default 'slide' AFTER `animation_speed`;
");

$installer->endSetup();