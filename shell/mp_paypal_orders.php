<!DOCTYPE html>
<html>
<head>
</head>
<body>
<?php
error_reporting(E_ALL); 
ini_set('display_errors', 1);

require_once '/var/www/html/app/Mage.php'; 
umask(0); Mage::app('default');

$collection = Mage::getModel('sales/order')->getCollection()
    ->join(
        array('payment' => 'sales/order_payment'),
        'main_table.entity_id=payment.parent_id',
        array('payment_method' => 'payment.method')
    );

$collection->addFieldToFilter('client_id',['neq' => 'NULL']);
//$collection->addFieldToFilter('increment_id', 100027494);
$collection->addFieldToFilter('ga_sent', array('null' => true));
$collection->addFieldToFilter('payment.method','paypal_standard')
->setOrder('entity_id', 'DESC')
->getSelect();
//->limit(10);

echo count($collection);
$ch = curl_init();

foreach ($collection as $order) {
    Mage::log('MP called for order=>'.$order->getIncrementId(),null,'mporders.log');
    $i=1;
    $items='';
    $cid = $order->getClientId(); 
    //echo "==".$cid."==";
    $ua = $order->getUserAgent();
    if($order->getTaxAmount()>0)
        $taxAmount = $order->getTaxAmount();
    else
        $taxAmount=0;
    //echo $taxAmount;exit;
    foreach ($order->getAllItems() as $item) { 
      //echo "Asdasd";
        $product = Mage::getModel('catalog/product')->load($item->getProductId());
        $prca = $product->getAttributeText('make')." ".$product->getAttributeText('model')."/".$product->getAttributeText('year')."/".$product->getAttributeText('enginesize');

        $items = $items."&pr".$i."qt=".(int)$item->getQtyOrdered()."&pr".$i."nm=".urlencode($item->getName())."&pr".$i."id=".urlencode($item->getSku())."&pr".$i."ca=".urlencode($prca)."&pr".$i."pr=".number_format($item->getPrice(),2, '.', '')."&pr".$i."va=".urlencode($product->getData('hardware'))."&pr".$i."br=".urlencode($product->getAttributeText('make'))."";
        $i++;     
    }
    if($taxAmount>0) 
        $tt = number_format($taxAmount, 2, '.', ''); 
    else 
        $tt=0;
    echo $getString = "v=1&t=pageview&tid=UA-57390566-1&cid=".urlencode(trim($cid))."&ni=1&ua=".urlencode($ua)."&uip=".$order->getRemoteIp()."&dl=https://www.fs1inc.com/checkout/order/success&dh=".urlencode('fs1inc.com')."&dp=".urlencode('/success')."&dt=Success&ta=".urlencode('Flagship One, Inc')."&pa=purchase&dt=".urlencode('Success Page')."&ti=".$order->getIncrementId()."&tr=".number_format($order->getGrandTotal(), 2, '.', '')."&tt=".$tt."&ts=".number_format($order->getShippingAmount(),2, '.', '')."".trim($items)."";

    Mage::log($getString,null,'mporders.log');
    //exit;
    curl_setopt($ch, CURLOPT_URL, 'https://www.google-analytics.com/collect?'.$getString);
    curl_setopt($ch, CURLOPT_HEADER, 0);

    curl_exec($ch);

    $info = curl_getinfo($ch);

    // TEST
    //echo 'CURL call : '.$getString.'<br />';
    // echo 'Response : <pre>';
    // print_r($info);
    // echo '</pre>';
?>
<?php 
  $order->setGaSent(1); 
  $order->save();
  //exit;  
}
curl_close($ch);
?>
</body>
</html>