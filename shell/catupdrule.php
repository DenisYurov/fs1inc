<?php
require_once '/var/www/html/app/Mage.php';
Mage::setIsDeveloperMode(true);
umask(0);
Mage::app('default');
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

try {
    Mage::getModel('catalogrule/rule')->applyAll();
    Mage::app()->removeCache('catalog_rules_dirty');
    echo Mage::helper('catalogrule')->__('The rules have been applied.');
} catch (Exception $exception) {
    echo Mage::helper('catalogrule')->__('Unable to apply rules.');
    echo $exception->getMessage();
}
?>
